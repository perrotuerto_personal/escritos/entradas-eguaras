# ¿*Single source* y *online publishing*, fuera de la realidad?

Para continuar con nuestro receso técnico, quiero hablar sobre el título en cuestión.
El principal interés es el deseo de seguir la discusión en los comentarios (espero
que esto no merme el deseo de publicarlos).

Como hemos visto a lo largo de diversas entradas, el método que se ha estado propuesto
como el más afín para satisfacer las necesidades editoriales actuales es el *single
source* y *online publishing* (SSOP). Por medio de esta metodología se pretende atajar
el «problema» que implica la producción de diversos formatos para una misma obra,
así como la pérdida de control en la edición al momento de usar una computadora.
(Para mayor detalle, lee «[Historia de la edición digital](http://marianaeguaras.com/historia-de-la-edicion-digital/)».)

Como toda propuesta, esta no está ausente de flaquezas y retractores, así que me
gustaría hacer algunas puntualizaciones y, ojalá, provocar que ustedes también den
su opinión.

## 1. El SSOP es demasiado técnico para el editor habitual

Al momento de explicar esta metodología dentro del medio editorial hay más de una
persona que simplemente le parece que requiere de mucho bagaje tecnológico, por
lo que parece estar destinada a la suerte de TeX: a un *supuesto* recoveco no apto
para la producción editorial habitual.

Mónica Braun, mi colega de Nieve de Chamoy y quien cuenta con una experiencia editorial
que sobrepasa mi edad (¡pero no por mucho!), es una de las más severas críticas
ante este método. Más de una vez ha sido motivo de discusión al momento de editar
y publicar los libros de Nieve de Chamoy. El uso de este método, opina junto con
otros, no está al alcance del editor que está habituado (¿mal acostumbrado?) al
uso del *software* que le provee Adobe o Microsoft.

*Ellos están en lo correcto*. El estado actual de esta metodología no es lo suficientemente
madura como para que en el presente reemplace a los procesos tradicionales de producción
editorial. A pesar de ello, quienes pueden ya implementar este método empiezan a
observar su pertinencia.

Por ejemplo, el primer libro de Nieve de Chamoy,
[*80 años: las batallas culturales del Fondo*](http://nievedechamoy.com.mx/tienda/index.php?route=product/product&product_id=51),
con más de 150,&#8201;000 palabras, 378 notas al pie y aproximadamente 70 fuentes
bibliográficas, nos tomó una semana producirlo a marchas forzadas en EPUB, dos o
hasta tres si hubiéramos tenido el tiempo suficiente. Actualmente, con esta metodología
y con las herramientas que en [Perro Triste](https://github.com/ColectivoPerroTriste/Herramientas)
hemos estado creando y afinando, la producción de este libro nos tomaría una jornada
de trabajo. No solo eso, la obra tendría una edición más cuidada, un mejor aspecto
estético y saldría para diversos formatos. (Actualmente estamos trabajando en la
segunda versión de los libros de Nieve de Chamoy.)

Además, también se olvida que en los procesos editoriales no solo el editor está
involucrado. El editor promedio por lo general nunca absorbe todos los procesos
y, *principalmente*, no tiene que hacerlo. Este método para mantener el control
en la edición sin importar la diversidad de formatos así como para agilizar los
tiempos de producción no necesariamente los tiene que aprender el editor, sino que
por lo general se orienta a otros perfiles profesionales como lo es el del diseñador
editorial, el tipógrafo o el desarrollador.

## 2. El SSOP no está pensado para los editores

*Sí y no*. El SSOP tiene su principal enfoque en afrontar una realidad que ha sobrepasado
a la edición tradicional: la publicación de diversos formatos con el mismo cuidado
y calidad editorial sin que esto aumente proporcionalmente la necesidad de recursos
y de tiempo.

Los procesos tradicionales de edición tienden a publicar formatos por ciclos, muy
a tono a la estrategia y la capacidad de la imprenta. Y esa manera de trabajar ha
sido transplantada a las recientes necesidades de la publicación digital. La contradicción
está anunciada: ¿por qué se inicia un proceso digital (la edición, el diseño e
incluso la impresión se llevan a cabo *a través de computadoras*) para obtener un producto
físico y luego *regresar* para producir un producto digital?

Como puede observarse, este problema no se origina por el quehacer tradicional del
editor. La corrección, la verificación, la inspección, el cuidado ortotipográfico,
y más no se ve amenazado por esos procesos *subsiguientes*. Sin embargo, sí puede
llegar a obstaculizarlo si el editor emplea formatos no adecuados (y que nunca fueron
pensados) para el cuidado editorial.

Si hacemos caso omiso a esta posibilidad, efectivamente el SSOP viene para cambiar
el paradigma cíclico y en su lugar implementar un modelo ramificado. El texto editado
es al SSOP lo que el tronco es al árbol, el elemento madre desde el cual se bifurca
para diversos formatos *al unísono y con mayor independencia entre una y otra rama*,
en lugar de iniciar esa bifurcación una vez que un formato ha sido concluido, y que
sin dudas hereda características (léase errores) del formato previo.

¿Peligro de una pérdida de control? Solo si el editor no cuida que su trabajo abandone
lo visual y se preocupe por el marcaje semántico del contenido. Además, con un controlador
de versiones, como los implementados por el uso de repositorios, siempre es posible
regresar a puntos de guardado sin la necesidad de preocuparnos por mantener un cuidado
(¿descuido por el típico respaldo, del respaldo, del respaldo?) en nuestro proyecto.

En fin, el SSOP si bine se orienta a esta bifucación posterior a la edición habitual,
requiere que el editor adopte este enfoque semántico, por lo que sí está pensado
para su trabajo, esencialmente para evitar la reproducción de vicios al momento
de trabajar con archivos digitales.

De nueva cuenta Mónica es un buen ejemplo de cómo un editor, al momento que deja
el enfoque visual y adopta la perspectiva semántica, mejora el cuidado de su trabajo.
Cuando utilizábamos directamente los archivos de procesador de texto se nos iban
muchas horas en limpiar ese formato, etiquetarlo y, principalmente, en ajustar debido
a diferentes criterios en la estructura que impactaban en el aspecto visual del
texto o en revisar que no se hubiera ido ni una sola itálica, por ejemplo, en toda
la obra. Ahora ella trabaja en directo con Markdown, así que ahora se encarga de
la cuestión semántica, por lo que el tiempo de producción ha disminuido de tal manera
que una de nuestras últimas obras publicadas, [Ukus](http://nievedechamoy.com.mx/tienda/index.php?route=product/product&product_id=50),
con casi 95,&#8201;000 palabras y apéndices, pero sin notas al pie o referencias
bibliográficas, fue desarrollada en media hora y los ajustes tuvieron el visto de
los dos en un día, ya que ella ya no tuvo que revisar palabra por palabra y yo no
me vi en la necesidad de preocuparme por tratar de homologar la estructura.

## 3. El SSOP requiere de un esfuerzo corporativo para mayor accesibilidad

Una de las principales debilidades actuales del SSOP, que no vale la pena negar,
es que no cuenta con un entorno amigable. Incluso, el SSOP carece de *un* entorno,
ya que por el momento se lleva a cabo a través de varios programas que en su mayoría
carecen de un entorno gráfico o, en el mejor de los casos, de un entorno amigable
para el usuario general. (Nótese cómo las necesidades editoriales se piensan como
necesidades *generales*.)

La mayoría de los usuarios estamos habituados a que con un solo *software* o con
una paquetería (varios programas relacionados unos con otros) se delimite nuestro
flujo de trabajo. Partiendo de esto, el SSOP requeriría de algún *software* o paquetería
que ayude al editor a cernirse a esta metdología.

El requerimiento no es disparatado, desde hace mucho tiempo he comentado la linda
idea de un Entorno de Edición Integrada (EEI). Este concepto no es original. En
el desarrollo de *software* existen los Entornos de Desarrollo Integrado (IDE, por
sus siglas en inglés) los cuales son un conjunto de herramientas bajo un mismo ambiente
de trabajo que pretende abarcar todas las necesidades que tiene un programador para
llevar a cabo su trabajo. No es, para nada, un entorno «amigable» para el usuario
promedio, pero sí es el que un desarrollador necesita.

Entonces, si un EEI es posible (¿EPI, Entorno de Publicación Integrada?), este debería
de satisfacer la mayoría de las necesidades de los procesos editoriales; es decir,
sería un entorno no solo para el editor, sino también para el diseñador, el tipógrafo
y el desarrollador. Para nada sería, de nueva cuenta, un ambiente «accesible» para
el usuario general, pero sí el que la edición necesita. Al final, el *software*
utilizado actualmente para la edición también carece de esa «amigabilidad», hasta
el punto donde actualmente hay editores que se niegan a aprender a usar InDesign,
por poner un ejemplo.

Una idea semejante ha estado trabajando Adobe a través del dichoso InDesign. No
es ningún secreto que este fue pensado para diseñadores editoriales de libros impresos,
pero que ahora también el editor lo utiliza y, bajo las necesidades actuales, asimismo
pretende ser una plataforma para la publicación digital al permitir la exportación
a EPUB o crear aplicaciones.

Como muchos de ustedes ya lo han experimentado, cuando se trata del trabajo del
editor o de la creación de otros formatos, InDesign se queda corto. Este *software*
*per se* no tiene contemplado necesidades de edición como son el cuidado en la uniformidad
o en lo ortotipográfico. Además, sin un conocimiento previo de tecnologías *web*,
se obtienen EPUB sin la calidad necesaria. Ya no digamos de las aplicaciones, sus
limitantes son por demás evidentes en comparación a un desarrollo con tecnologías
nativas, híbridas o a través de motores de videojuegos.

Por este gran esfuerzo necesario para cuajar todo un nuevo método en un solo entorno
varias personas opinan que una inversión corporativa es necesaria, como Adobe desarrollando
un nuevo *software* para suplir a InDesign en este nuevo panorama (podría decir
que no lo dudo, Adobe también es conocido por descontinuar sus productos en aras
de continuar en la «vanguardia»). Y solo a través de esta inversión sería posible
tener en relativamente poco tiempo un «producto», que ya no «método», para cumplir
satisfactoriamente con las necesidades de la producción editorial.

Sobre el futuro no puedo decir nada. Pero viendo el panorama, es cierto que el desarrollo
no corporativo toma más tiempo, ya que por lo general es trabajo realizado en horas
libres; es menos conocido, porque carece de una infraestructura con poder mediático;
así como se enfoca a la creación de una diversidad de herramientas en lugar de un
entorno que lo englobe todo, debido a que ese «todo» nunca es claro ya que cada
usuario utiliza las herramientas según más le convenga y no bajo una idea de aceptación
general sobre *cómo* se tiene que trabajar.

Sin embargo, también es cierto que el trabajo de las comunidades abiertas de desarrollo
tiene más vida útil, TeX tiene más de treinta años y el HTML más de veinte, ¿cuánto
duró PageMaker o la hegemonía de QuarkXPress?; más flexibilidad de personalización
y, claro está, la característica de prescindir de un pago obligatorio para su uso.

Llámenme iluso o soñador, pero si de accesibilidad hablamos, me cuesta mucho trabajo
comprender su posibilidad cuando la neutralidad es casi inexistente, cuando es una
estructura corporativa la que toma las decisiones. ¿Qué pasará cuando Adobe anuncie
la descontinuación de InDesign? ¿Desaprender para aprender a utilizar otro *software*?
La descontinuación no es infrecuente en el desarrollo de *software*, pero en comunidades
abiertas y estables de desarrollo no tienden a pasar cambios abruptos, sino una
lenta y continúa evolución. Quien aprendió TeX desde los ochenta poco o nada ha
tenido que «actualizarse». Quien empezó a usar HTML en los noventas, las nuevas
versones le han dado más posibilidades de desarrollo…
