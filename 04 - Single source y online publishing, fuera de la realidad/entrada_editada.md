# 4. ¿_Single source_ y _online publishing_, fuera de la realidad?

Para continuar con nuestro receso técnico, quiero hablar sobre el título
de esta entrada: **¿_Single source_ y _online publishing_, fuera de la
realidad?** Mi principal interés es seguir la discusión de los
argumentos aquí propuestos en los comentarios (y espero que esto no
merme el deseo de publicarlos).

Como hemos visto a lo largo de diversas entradas, el método que se ha
propuesto como el más afín para satisfacer las necesidades editoriales
actuales es el _single source_ y _online publishing_ (expresiones que
trataremos como una entidad única, y cuyo acrónimo es +++SSOP+++).

Por medio de esta metodología se pretende **atajar el «problema» que
implica la producción de diversos formatos para una misma obra**, así
como la **pérdida de control en la edición** al momento de usar una
computadora. (Para mayor detalle, lee «[Historia de la edición
digital](@index[2])»).

Como toda propuesta, esta no está ausente de flaquezas y detractores.
Por tanto, me gustaría hacer algunas puntualizaciones y, ojalá, provocar
que tú también des tu opinión.

## 1. El _single source_ y _online publishing_ es demasiado técnico para el editor

Cuando se intenta explicar el uso de TeX al medio editorial más de un
editor lo considera tecnológicamente complicado. TeX es percibido como
no apto para los tiempos y los requerimientos que manejan en sus libros.
Algo semejante sucede con el _single source_ y _online publishing_: a los
editores no les parece que sea técnicamente viable, ya que implica un
cambio en la manera de editar sus libros.

Mónica Braun, mi colega de Nieve de Chamoy ---y quien cuenta con una
experiencia editorial que sobrepasa mi edad (¡pero no por mucho!)---, es
una de las más severas críticas de este método. Más de una vez ha sido
el motivo de discusión para editar y publicar los libros de Nieve de
Chamoy. El uso de este método ---opina junto con otros--- no está al
alcance del editor que está habituado (*¿mal acostumbrado?*) al uso del
_software_ que le provee Adobe o Microsoft.

*Ellos están en lo correcto*. El estado actual de esta metodología no es
lo suficientemente madura como para que en el presente reemplace a los
procesos tradicionales de producción editorial. A pesar de ello, quienes
pueden ya implementar este método empiezan a observar su pertinencia.

Por ejemplo, el primer libro de Nieve de Chamoy, _80 años: las batallas
culturales del Fondo_,
con más de 150.000 palabras, 378 notas al pie y aproximadamente 70
fuentes bibliográficas, nos tomó una semana producirlo en +++EPUB+++,
y porque lo hicimos a marchas forzadas; dos o hasta tres semanas si hubiéramos
tenido el tiempo suficiente.

Actualmente, con el +++SSOP+++ y con las herramientas que en [Perro
Triste](https://github.com/NikaZhenya/Pecas) hemos
estado creando y afinando, la producción de este libro **nos tomaría
solo una jornada de trabajo**. No solo eso, la obra tendría una
**edición más cuidada, un mejor aspecto estético y con salida para
diversos formatos**. (Actualmente, estamos trabajando en la segunda
versión de los libros de Nieve de Chamoy).

Además, se suele olvidar que en los procesos editoriales no solo el
editor está involucrado. El editor promedio, por lo general, nunca
absorbe todos los procesos y, *principalmente*, no tiene que hacerlo.

Este método para mantener el control en la edición, sin importar la
diversidad de formatos así como para agilizar los tiempos de producción,
no necesariamente los tiene que aprender el editor, sino que está
orientado a otros perfiles profesionales, como los del diseñador
editorial, el tipógrafo o el desarrollador.

## 2. El _single source_ y _online publishing_ no está pensado para los editores

*Sí y no*. El _single source_ y _online publishing_ tiene su principal
enfoque en afrontar una realidad que ha sobrepasado a la edición
tradicional: la **publicación de diversos formatos con el mismo cuidado
y calidad editorial sin que esto aumente proporcionalmente la necesidad
de recursos y de tiempo**.

Los procesos tradicionales de edición tienden a publicar formatos por
ciclos, muy a tono con la estrategia y la capacidad de la imprenta. Y
esa manera de trabajar ha sido trasplantada a las recientes necesidades
de la publicación digital.

La contradicción está anunciada: **¿por qué se inicia un proceso
digital** (la edición, el diseño e incluso la impresión se llevan a cabo
*a través de computadoras*) **para obtener un producto físico y luego
*regresar* para producir un producto digital?**

Como puede observarse, este problema no se origina por el quehacer
tradicional del editor. La
[corrección](https://marianaeguaras.com/correccion-de-estilo-y-ortotipografica-diferencias/),
la verificación, la inspección, el cuidado ortotipográfico y más, no se
ven amenazados por esos procesos *subsiguientes*. Sin embargo, sí pueden
llegar a obstaculizarlos si el editor emplea formatos no adecuados (y
que nunca fueron pensados) para el cuidado editorial.

Si hacemos caso omiso a esta posibilidad, efectivamente el _single
source_ y _online publishing_ viene para cambiar el paradigma cíclico y,
en su lugar, implementar un **modelo ramificado**. El texto editado es
al _single source_ y _online publishing_ lo que el tronco es al árbol.
**El contenido editado es el elemento madre desde el cual se obtienen
diversos formatos y a partir de él se bifurcan distintas ramas**; es
decir, se obtienen distintos formatos de salida (+++EPUB+++,
+++HTML+++, +++MOBI+++, +++PDF+++, etc.).

Este proceso se realiza de manera simultánea y con mayor independencia
entre una y otra rama. Se evita que esta bifurcación se produzca una vez
que un formato ha sido concluido y se elude la herencia de las
características (léase errores) del formato previo.

¿Peligro a la pérdida de control? Solo si el editor continúa dándole
preferencia al aspecto visual, en lugar de preocuparse por el **marcaje
semántico del contenido**. Además, con un controlador de versiones, como
los implementados por los repositorios, siempre es posible regresar a
puntos de guardado anteriores, sin necesidad de preocuparnos por el
típico respaldo, del respaldo, del respaldo.

En fin, el _single source_ y _online publishing_ si bien se orienta a
esta bifucación posterior a la edición habitual, requiere que el editor
adopte este enfoque semántico. Por tanto, sí está pensado para su
trabajo y, esencialmente, para **evitar la reproducción de vicios al
momento de trabajar con archivos digitales**. De nueva cuenta, Mónica es
un buen ejemplo de cómo un editor, al momento que deja el enfoque visual
y adopta la perspectiva semántica, mejora el cuidado de su trabajo.

Cuando utilizábamos directamente los archivos del procesador de texto se
nos iban muchas horas en limpiar ese formato y etiquetarlo. Pero,
principalmente, en ajustarlo, debido a diferentes criterios en la
estructura que impactaban en el aspecto visual del texto o, por ejemplo,
en revisar que no se hubiera escapado ni una sola itálica en toda la
obra.

Ahora Mónica trabaja directamente con Markdown y así se encarga de la
cuestión semántica. Esto ha hecho que el tiempo de producción haya
disminuido. Una de nuestras últimas obras publicadas,
_Ukus_, con casi 95.000 palabras y apéndices 
(sin notas al pie o referencias bibliográficas), fue desarrollada en 
media hora. Ambos dimos el visto bueno a los ajustes en un día: ella 
no tuvo que revisar palabra por palabra y yo no me vi en la necesidad 
de preocuparme por tratar de homologar la estructura.

## 3. El _single source_ y _online publishing_ requiere de un esfuerzo corporativo para mayor accesibilidad

Una de las principales **debilidades actuales del _single source_ y
_online publishing_** ---que no vale la pena negar--- es que no cuenta
con un entorno amigable. Incluso, el +++SSOP+++ carece de *un* entorno,
ya que por el momento se lleva a cabo a través de varios programas. Y estos, en
su mayoría, carecen de un entorno gráfico o, en el mejor de los casos,
de un entorno amigable para el usuario general. (Nótese cómo las
necesidades editoriales se piensan como necesidades *generales*).

La mayoría de los usuarios estamos habituados a que con un solo
_software_ o con una paquetería (varios programas relacionados unos con
otros) se delimite nuestro flujo de trabajo. Partiendo de esto, **el
_single source_ y _online publishing_ requeriría de algún _software_ o
paquetería que ayude al editor a cernirse a esta metodología**.

El requerimiento no es disparatado, desde hace mucho tiempo he comentado
la linda idea de un **Entorno de Edición Integrada (+++EEI+++)**. Este
concepto no es original. En el desarrollo de _software_ existen los
Entornos de Desarrollo Integrado (+++IDE+++, por sus siglas en inglés)
los cuales son un conjunto de herramientas bajo un mismo ambiente de trabajo
que pretende abarcar todas las necesidades que tiene un programador para
llevar a cabo su trabajo. No es, para nada, un entorno «amigable» para
el usuario promedio, pero sí es el que un desarrollador necesita.

Entonces, si un Entorno de Edición Integrada es posible (¿+++EPI+++,
Entorno de Publicación Integrada?), este debería satisfacer la mayoría de las
necesidades de los procesos editoriales. Es decir, sería un entorno no
solo para el editor, sino también para el diseñador, el tipógrafo y el
desarrollador.

Para nada sería, de nueva cuenta, un ambiente «accesible» para el
usuario general, pero sí el que la edición necesita. Al final, el
_software_ utilizado actualmente para la edición también carece de esa
«amigabilidad», hasta el punto donde actualmente hay editores que se
niegan a aprender a usar
[InDesign](https://marianaeguaras.com/usas-indesign-sacale-mas-partido-con-estas-entradas/),
por poner un ejemplo.

Una idea semejante ha estado trabajando Adobe a través del dichoso
InDesign. No es ningún secreto que este fue pensado para diseñadores
editoriales de libros impresos, pero que ahora también el editor lo
utiliza y, bajo las necesidades actuales, asimismo pretende ser una
plataforma para la publicación digital, ya que permite la exportación a
+++EPUB+++ o crear aplicaciones.

Como muchos hemos experimentado, cuando se trata del trabajo del editor
o de la creación de otros formatos, InDesign se queda corto. Este
_software per se_ no contempla necesidades de edición como son el
cuidado en la uniformidad o el aspecto ortotipográfico.

Además, sin un conocimiento previo de tecnologías _web_, se obtienen
+++EPUB+++ sin la calidad necesaria (véase De [+++XML+++ a
+++EPUB+++ con InDesign](@index[3])).
Ya no digamos de las aplicaciones: sus limitaciones son por demás evidentes en
comparación a un desarrollo con tecnologías nativas, híbridas o a través
de motores de videojuegos.

Por este gran esfuerzo ---necesario para cuajar todo un nuevo método en
un solo entorno--- varias personas opinan que **una inversión
corporativa es necesaria**. Por ejemplo, Adobe desarrollando un nuevo
_software_ para suplir a InDesign en este nuevo panorama (podría decir
que no lo dudo, Adobe también es conocido por descontinuar sus productos
en aras de continuar en la «vanguardia»). Y solo a través de esta
inversión sería posible tener, en relativamente poco tiempo, un
«producto» ---que ya no «método»--- para cumplir satisfactoriamente con
las necesidades de la producción editorial.

Sobre el futuro no puedo decir nada. Pero viendo el panorama, es cierto
que el desarrollo no corporativo toma más tiempo. Por lo general, es
trabajo realizado en horas libres; es menos conocido, porque carece de
una infraestructura con poder mediático.

Asimismo, se enfoca en la creación de una diversidad de herramientas en
lugar de un entorno que lo englobe todo, debido a que ese «todo» nunca
es claro ya que cada usuario utiliza las herramientas según más le
convenga y no bajo una idea de aceptación general sobre *cómo* se tiene
que trabajar.

Sin embargo, también es cierto que el **trabajo de las comunidades
abiertas de desarrollo** tiene más vida útil. TeX tiene más de treinta
años y el +++HTML+++ más de veinte. ¿Cuánto duró sino la hegemonía de
Quark+++XP+++ress? Más flexibilidad de personalización y, claro está, la
característica de prescindir de un pago obligatorio para su uso.

Llámenme iluso o soñador, pero si de accesibilidad hablamos, me cuesta
mucho trabajo comprender su posibilidad cuando la neutralidad es casi
inexistente, cuando es una estructura corporativa la que toma las
decisiones.

**¿Qué pasará cuando Adobe anuncie la descontinuación de InDesign?
¿Desaprender para aprender a utilizar otro _software_?** La
descontinuación no es infrecuente en el desarrollo de _software_, pero
en comunidades abiertas y estables de desarrollo no tienden a cambios
abruptos, sino una lenta y continua evolución. Quien aprendió TeX desde
los ochenta poco o nada ha tenido que «actualizarse». Quien empezó a
usar +++HTML+++ en los noventa, las nuevas versiones le han dado más
posibilidades de desarrollo.

<aside class="espacio-arriba2">

###### Sobre esta entrada

* Título original: «¿_Single source_ y _online publishing_, fuera de la realidad?».
* Título en el _blog_ de Mariana: «¿Single source y online publishing, fuera de la realidad?».
* Fecha de publicación: 19 de diciembre del 2016.
* [Redacción original](https://gitlab.com/NikaZhenya/entradas-eguaras/blob/master/04%20-%20Single%20source%20y%20online%20publishing,%20fuera%20de%20la%20realidad/entrada.md).
* [Revisión de Mariana](https://marianaeguaras.com/single-source-online-publishing-la-realidad/).

</aside>
