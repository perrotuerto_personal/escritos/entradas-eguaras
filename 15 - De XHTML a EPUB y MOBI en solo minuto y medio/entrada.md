# De XHTML a EPUB y MOBI ¡en solo minuto y medio!

> En el siguiente artículo se utiliza [Pecas](https://github.com/NikaZhenya/Pecas),
> un conjunto de herramientas editoriales que aún están en desarrollo. Existen
> diferencias sustanciales en su uso desde [la primera vez que se mencionó como «edición desde cero»](http://marianaeguaras.com/edicion-digital-como-edicion-desde-cero/).
> Su uso probablemente también varíe en el futuro.

## El último *Don Quijote*

Desde que comencé a colaborar en este *blog* hemos estado haciendo al *Don Quijote*
con distintas herramientas.

* [Primero empezamos con un análisis general](http://marianaeguaras.com/edicion-digital-como-edicion-desde-cero/)
  donde se explicaba que el método «desde cero» es el más pertinente para un
  quehacer editorial controlado.
* [Después explicamos cómo hacer un EPUB con InDesign](http://marianaeguaras.com/de-xml-a-epub-con-indesign/).
* [Por último mostramos cómo crearlo a partir de Sigil](https://marianaeguaras.com/de-xhtml-a-epub-con-sigil/).

Hoy es el turno del último modo de producción: el método desde cero. Pero ¿qué 
es eso de «desde cero»? Muy sencillo: meternos de lleno con el código de nuestro
libro. **Esto no implica que tengamos que conocer la estructura del EPUB**
—aunque es recomendable—, ya que es posible usar una serie de [*scripts*](https://es.wikipedia.org/wiki/Script)
que nos permiten concentrarnos solo en el contenido del libro.

## ¿Qué es Pecas?

Los *scripts* que utilizo y desarrollo para hacer cualquier tipo de publicación 
son herramientas libres disponibles [aquí](https://github.com/NikaZhenya/Pecas).
En conjunto se llaman Pecas y la intención es crear herramientas editoriales con 
lo siguiente en mente:

* Desarrollo multiformato para obtener por lo menos archivos EPUB, MOBI, PDF
  para *ebook* y PDF para impresión a partir de archivos madre. Es lo que en
  [otras publicaciones](https://marianaeguaras.com/edicion-ciclica-y-edicion-ramificada-de-la-antesala-a-la-revolucion-digital/)
  se ha llamado «edición ramificada».
* Herramientas de control editorial y no solo para la producción de formatos.
* Formatos que se adapten a la mayoría de las exigencias posibles de la 
  tradición editorial en español.

**Las herramientas por el momento carecen de entorno gráfico**, por lo que se
usan desde la [terminal](https://es.wikipedia.org/wiki/Emulador_de_terminal).
Además son multiplataforma, por lo que no importa si andas sobre Windows, macOS
o Linux —en Windows recomiendo instalar un [WSL con Ubuntu](https://msdn.microsoft.com/es-MX/commandline/wsl/faq) 
para mayor comodidad.

Como puede verse, es un proyecto muy ambicioso que ya lleva casi año y medio de
vida. A estas alturas el área de publicación digital está ya casi terminada
como podemos ver en este gráfico:

![Proceso para la producción de EPUB.](img/img1.jpg)

A partir de un archivo madre en Markdown o HTML es posible arrancar con la
creación de un proyecto EPUB (`creator.rb`), dividirlo en secciones (`divider.rb`),
añadirle notas al pie (`notes.rb`), producir el EPUB (`recreator.rb`), convertirlo
a otras versiones de EPUB (`changer.rb`) e incluso generar un MOBI si se cuenta
con un programa desarrollado por Amazon (`kindlegen`). Aún está pendiente el
manejo de referencias bibliográficas (`cites.rb`) y la creación de índices
analíticos o glosarios (`index.rb`).

Para los afortunados que no tienen una manía por el control de cada uno de los
procesos, existe la posibilidad de automatizarlos para hacer todo con un solo 
comando (`automata.rb`). Con este libro procederemos de esta manera y
**en un minuto y medio produciremos dos archivos EPUB y un MOBI validados**.

## Archivos de entrada

> Ojo: todos los archivos están disponibles [aquí](http://alturl.com/79qtb).

Como hemos procedido en los otros métodos, nuestros archivos iniciales —ubicados 
en `don_quijote/desde_cero/archivos_madre`— son los siguientes:

* `todo.xhtml`. El contenido del libro.
* `portada.jpg`. La portada del *Don Quijote*.
* `principal.css`. Los estilos que usaremos para esta edición.

Estos son los archivos madre, los cuales ya no tenemos necesidad de modificar
porque ya le hemos dado el formato adecuado. [Ya nos ahorramos ese dolor de cabeza](http://marianaeguaras.com/el-formato-de-una-publicacion-cuello-de-botella-en-la-edicion/),
donde el cuidado en **el formato es esencial para el control editorial de cualquier publicación, impresa o digital**.

## ¡Manos a la obra!

Ubicados en `don_quijote/desde_cero` solo usamos el comando `pc-automata` para
llamar a `automata.rb`, que a su vez llamará al resto de los *scripts* y los
programas:

![Producción de dos EPUB y un MOBI en minuto y medio.](img/video1.gif)

### 1. Creando el proyecto EPUB

El primer comando es para crear una carpeta en donde las herramientas trabajarán:

```
pc-automata --init
```

Esto nos crea una carpeta llamada `epub-automata` el cual solo contiene dos
ficheros:

* `.automata_init`. Un archivo oculto que permite validar la carpeta como un
  directorio para lo que hará `pc-automata`.
* `automata_meta-data.yaml`. Un archivo donde pondremos los metadatos del libro.

### 2. Agregando los metadatos

El siguiente paso es abrir el archivo `automata_meta-data.yaml` para introducir
los metadatos. Este fichero puede abrirse con cualquier programa; por ejemplo
Bloc de Notas, TextEdit o Gedit. Por la grabación utilicé `nano`, un editor de 
texto que se usa desde la terminal: 

```
nano epub-automata/automata_meta-data.yaml
```

### 3. Creación de los EPUB y el MOBI

Con nuestro proyecto y metadatos listos, solo es necesario escribir este comando:

```
pc-automata -f archivos_madre/todo.xhtml -c archivos_madre/portada.jpg -s archivos_madre/principal.css -d epub-automata/ -y epub-automata/automata_meta-data.yaml --section
```

Pero ¿qué significa todo eso? Vayámonos por partes para observar que no es nada
complejo:

* `pc-automata`. Con esto se manifiesta que llamaremos a `automata.rb` para 
  producir los EPUB y el MOBI de manera automatizada.
* `-f archivos_madre/todo.xhtml`. Este primer parámetro señala cuál es el 
  archivo (***f**ile*) que contiene todo el libro.
* `-c archivos_madre/portada.jpg`. Con este otro indicador se menciona cuál es
  la portada (***c**over*) de la obra.
* `-s archivos_madre/principal.css`. Aquí se enlaza la hoja de estilos 
  (***s**tyle sheet*) de la publicación.
* `-d epub-automata/`. Si no estamos adentro de la carpeta (***d**irectory*) 
  del proyecto, se tiene que indicar dónde se encuentra.
* `-y epub-automata/automata_meta-data.yaml`. Como no estamos adentro del 
  directorio, también hay que señalar donde está el archivo **Y**AML que 
  contiene los metadatos.
* `--section`. Por último, se indica que la división del documento no se hará
  cada etiqueta `<h1>` sino cada `<section>`.

Con esto —y como podemos ver en `don_quijote/desde_cero/epub_automata/log.txt`—
`pc-automata` lleva a cabo estas tareas:

1. Crea un proyecto EPUB con `pc-creator`.
2. Divide el archivo `todo.xhtml` cada etiqueta `<section>` con `pc-divider`.
3. Crea el EPUB versión 3.0.1 —todavía no lo actualizo a 3.1— con `pc-recreator`.
4. Crea el EPUB versión 3.0.0 a partir del EPUB 3.0.1 con `pc-changer` —la
   versión 3.0.0 es la más popular y la aceptada por terceros como Amazon,
   iTunes y Google Play.
5. Se verifica el EPUB 3.0.1 con `epubcheck`, la herramienta oficial de 
   validación que es mantenida por el [International Digital Publishing Forum](https://github.com/IDPF/epubcheck).
6. Se verifica el EPUB 3.0.0 también con `epubcheck` —si hay errores, en la 
   terminal lo indicará y quedará guardado en el archivo `log.txt`.
7. Si se cuenta con `kindlegen`, termina los procesos creando el MOBI a partir
   del EPUB 3.0.1.

## Primer resultado

Con esto se crean los EPUB y el MOBI cuyo resultado son:

![Portada del EPUB.](img/img2.jpg)

Como puede verse, los estilos están bien aplicados:

![Interior del EPUB.](img/img3.jpg)

Y por supuesto, automáticamente se creó la tabla de contenidos:

![Tabla de contenidos del EPUB.](img/img4.jpg)

¡Momento! En la tabla de contenidos todas las secciones tienen la misma 
jerarquía… Por experiencia esto no incomoda a la mayoría de las personas. Sin
embargo, hay ocasiones donde se quiere ocultar elementos —como la legal— o
crear una tabla de contenidos jerarquizada.

No hay ningún problema, es solo cuestión de modificar nuestros metadatos. Si
se abre de nuevo el archivo `automata_meta-data.yaml` se observa el área para
la tabla de contenidos, donde la opción más personalizable es el área de `custom:`.
Desde ahí acomodamos los elementos que deseamos mostrar en la tabla, metiendo
unos más que otros para jerarquizarlos:

![Creación de una tabla de contenidos jerárquico.](img/img5.jpg)

## Segunda recreación

Con la tabla de contenidos modificada, solo es necesario volver a correr el
comando para producir los archivos. La sintaxis es la misma a cuando 
desarrollamos los formatos por primera vez:

```
pc-automata -f archivos_madre/todo.xhtml -c archivos_madre/portada.jpg -s archivos_madre/principal.css -d epub-automata/ -y epub-automata/automata_meta-data.yaml --section
```

El proceso vuelve a comenzar sin ninguna dificultad:

![Nueva producción de los EPUB y el MOBI.](img/video2.gif)

¡Ya tenemos de nuevo nuestros libros! Con una hermosa tabla de contenidos:

![EPUB con una tabla de contenidos jerárquica.](img/img6.jpg)

Que puede expandirse para ver el resto de las secciones:

![EPUB con una tabla de contenidos expandida.](img/img7.jpg)

## Dos curiosidades

Como se observó en la segunda recreación, el proceso de automatización sigue una
idea de desecho: todos los archivos que no sean los metadatos o archivos TXT son 
eliminados para volver a generar todos los ficheros. ¿Por qué? Por un lado 
porque la generación de archivos puede llegar a confundir al usuario si se 
mezclan con ficheros antiguos. Por el otro, la idea metodológica es el trabajo 
con los archivos madre, así que en caso de modificación, ¿por qué se querría 
utilizar con los ficheros generados en lugar de modificar los archivos madre? 
Con esto **es posible no preocuparnos por los formatos finales**, a tal grado 
que incluso no es necesario subirlos al servidor, ya que en cualquier momento se 
pueden volver a generar a partir de los archivos madre. 

Así nos ahorramos un poco más de espacio y nos quitamos la preocupación de estar 
respaldando los archivos finales a cada instante. El control es tal, que 
**la mala práctica de tener los últimos cambios en los archivos finales no tiene cabida**.

¿Es necesario hacer modificaciones que con la automatización no se pueden llevar
a cabo? Aunque se trata de una singularidad, la posibilidad no queda eliminada, 
ya que `pc-automata` deja el proyecto EPUB sin compresión en la carpeta 
`epub-automata/epub-creator`. La mayoría de los casos no se recomienda trabajar 
con esos archivos, pero la flexibilidad permanece porque entre menos control hay 
en un proyecto editorial, más lugar hay para las excepciones…

## ¿Quieres probar Pecas en modo automatizado?

Si tienes curiosidad de probar Pecas como se hizo aquí, puedes recrear este
ejercicio. Solo es necesario:

* [Descargar los archivos del Don Quijote](http://alturl.com/79qtb) —elimina la carpeta `epub-automata`.
* [Instalar todo lo necesario para Pecas](https://github.com/NikaZhenya/cursitos/tree/master/instalacion).

También existe otro ejercicio adicional en el cual puedes crear los EPUB y el
MOBI desde un archivo PDF y pasando por Markdown. Para acceder solo da [clic aquí](https://github.com/NikaZhenya/cursitos/tree/master/modulos/practicos/P003-Pecas).
