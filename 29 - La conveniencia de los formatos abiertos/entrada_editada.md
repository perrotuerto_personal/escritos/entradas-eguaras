# 29. La conveniencia de los formatos abiertos para la industria editorial

Durante la producción de una publicación, por lo general, nos
encontramos con inconvenientes que, aunque se traten de proyectos
distintos, parecen ser los mismos problemas. Algunas de estas
dificultades son:

-   Recuperar archivos que fueron elaborados con programas en desuso o
    en versiones anteriores ya incompatibles.
-   Mantener el _software_ actualizado, que en varios casos implica el
    pago de cuotas para tener acceso a las nuevas versiones e incluso
    cambiar el equipo de cómputo.
-   Usar un programa para trabajar con soportes o formatos específicos.
-   Volver a formatear el documento cada vez que se edita en otro
    programa o en una versión más reciente del mismo _software_.
-   Actualizar los archivos para evitar problemas en su acceso o su uso.

Con demasiada normalidad pensamos que los siguientes trabajos
siempre tienen que hacerse:

-   Adquirir _plugins_ para poder reeditar en InDesign viejas ediciones
    hechas con QuarkXPress u otros programas.
-   Actualizar InDesign constantemente para poder usarlo y, en tres o
    cinco años, adquirir un nuevo _hardware_.
-   Ignorar u obviar que para producir +++PDF+++ o trabajar con texto
    procesado hay alternativas con la misma calidad a InDesign o
    Microsoft Office.
-   Dar formato al documento +++DOCX+++ para luego volver a formatearlo
    en InDesign y, de nueva cuenta, dar formato para desarrollar un
    +++EPUB+++.
-   Actualizar archivos de InDesign para poderlos usar en nuevas
    versiones del programa.

## ¿Qué es un formato «abierto»?

__Aunque parezca una nimiedad, poco a poco el mantenimiento de
los archivos acaba siendo una tarea que consume mucho tiempo.__
Por regla general, entre más viejo es un proyecto más horas son
necesarias para la preparación de una nueva edición. Los recursos
empleados también se incrementan cuando se trata de generar múltiples
formatos y no solo un libro impreso.

Son escenarios que se dan sin importar si se usa _software_ libre
o de código abierto, o programas de pago. Sin embargo, el mantenimiento
se vuelve más sencillo cuando, además de tener la libertad de
elección de programas y metodologías de trabajo, los archivos
se encuentras en un formato accesible.

__Un formato «abierto» es aquel que permite su uso y modificación
con el programa de nuestra elección.__ Cuando un archivo está
en formato abierto, podemos elegir qué programa emplear para
utilizarlo. Por ejemplo, los formatos +++HTML+++, +++XML+++,
+++IDML+++, +++PDF+++, +++EPUB+++, +++ODT+++ y +++DOCX+++ son
todos abiertos.

Aunque la metodología editorial común es usar Microsoft Word
para archivos +++DOCX+++ e InDesign para producir +++PDF+++ para
impresión estos formatos pueden trabajarse o generarse con otros
programas como [LibreOffice](https://es.libreoffice.org/), [Scribus](https://www.scribus.net/)
o [TeX](https://tug.org/).

Por otro lado, __un formato «cerrado» es aquel que solo puede
usarse con un programa en específico__. La mayoría de los casos
implica el pago por el _software_ necesario.

Los formatos +++INDD+++ y +++DOC+++ son formatos que nada más
pueden usarse con InDesign o Microsoft Word sin violar derechos
de autor.

## Adopción de los formatos abiertos por el sector editorial

__En la actualidad, y casi sin notarlo, la industria editorial
ha ido adoptando estándares de formatos abiertos.__ Esto se debe
a cuestiones de diseño del programa o a una tensión constante
en el desarrollo de _software_.

Por defecto, ahora Microsoft Word guarda los documentos en el
formato abierto +++DOCX+++, una decisión en su diseño que muchos
de sus usuarios no se dieron cuenta.

Dentro de un ecosistema de desarrollo de programas de cómputo
propietarios, existe la constante necesidad de generar nuevas
versiones para obligar al usuario a comprarlos y así mantener
el negocio. Un efecto de este modo de hacer _software_ es que
un mismo programa genera diferentes tipos de formato según su
versión, lo que provoca incompatibilidades.

Esto obliga a los desarrolladores a recurrir a formatos abiertos
para poder trasladarse entre versiones. Un ejemplo de ello es
la generación de archivos [+++IDML+++](https://maquetatulibro.com/que-es-un-archivo-idml-como-gestionarlo-y-para-que-sirve/)
para que usuarios con distintas versiones de InDesign puedan
trabajar con el mismo documento.

Siempre es recomendable revisar los formatos abiertos disponibles
para cada tipo de proyecto. Aunque __una manera más fácil de
averiguar si un archivo está en formato abierto es intentar abrirlo
con otro programa, sin pérdida de información y sin violar derechos
de autor.__

## Longevidad de los archivos

Quizá suene extraño, pero __los archivos también perecen sin
su correcto mantenimiento__. Una idea muy extendida en el contexto
digital es que los archivos son atemporales y basta con mantenerlos
respaldados.

¿Cuántas veces al momento de empezar una reedición o retomar
un proyecto nos damos cuenta de que no es así? La velocidad de
actualización del _software_ en varios casos implica la necesidad
de mantener los archivos en el formato más reciente para evitar
pérdida de acceso o de información.

__En formatos cerrados esto es un potencial dolor de cabeza__.
Por cuestión de propiedad intelectual solo el titular de los
derechos tiene la capacidad legal de mantener los programas necesarios
para el uso de estos formatos. Pero en más de un caso sucede
que el titular cesa de mantener el _software_, lo que causa que
los usuarios tengan que migrar de programa y recurrir a terceros
para poder realizar la conversión entre formatos.

En más de una ocasión esta migración implica pérdida de información.
En algunas situaciones la migración falla o la calidad es tan
pobre que es menester volver a hacer todo de nuevo.

Con formatos abiertos este problema es casi inexistente. __Al
poderse abrir el archivo en formato abierto con cualquier programa,
el usuario no depende de las compañías de _software_.__

Si un desarrollador cesa de darle mantenimiento a un programa,
el usuario puede optar por usar otro _software_ para continuar
su trabajo. Esta migración de programas y no de formatos tiende
a no ser agradable porque, quizá, necesitemos emplear ciertas
horas para aprender la nueva interfaz. Sin embargo, se trata
de un asunto relativo a la comodidad del usuario y no en relación
con la conservación de la información.

## Soberanía tecnológica

Aunque __el concepto de «soberanía tecnológica» es__ [amplio](https://es.wikipedia.org/wiki/Soberanía_tecnológica),
podemos definirla como __el empoderamiento y la autonomía de
los usuarios en el uso y el desarrollo tecnológicos__. Esta clase
de soberanía implica la libertad de elección por parte de los
usuarios. Pero ¿elección de qué?

__En al menos tres rubros los usuarios pueden declarar su autonomía.__

### 1. Libertad de uso de programas

En lugar de estar obligado a adoptar los «estándares» de la industria,
el usuario puede optar por el programa que considere más conveniente
para realizar su trabajo. En más de una ocasión en el mundo editorial
se tiende a hablar de Adobe como la paquetería de _software_
estándar en la edición.

No obstante, se pasa por alto que un programa no puede considerarse
un estándar si no es accesible para cualquier usuario. Es decir,
__el establecimiento de estándares requiere la posibilidad de
su adopción sin ningún intermediario__.

En la industria editorial se habla de estándar solo porque la
mayoría de los editores usan los mismos programas, sin importar
que esto requiera un constante pago por su uso y una dependencia
tecnológica hacia una compañía.

Eso no es un «estándar» sino __la hegemonía y el monopolio__
de una industria por parte de un tercero cuya actividad económica
es de distinta índole. __Decir que Adobe es el «estándar» en
la edición es lo mismo a asentir que una compañía desarrolladora
de _software_ tiene el poder de decidir el rumbo de la industria
editorial, una actividad que le es ajena.__

Además, hay que recordar que Adobe surgió para ofrecer soluciones
de diseño gráfico, incluyendo diseño editorial, y no para vender
a los editores una paquetería de _software_ para realizar su
trabajo.

### 2. Libertad metodológica

En [otra ocasión](@index[11]) mencioné las implicaciones de confundir
un programa de cómputo con un método de trabajo. Cuando el uso
hegemónico de una paquetería de _software_, como Microsoft Office
o Adobe Creative Suite, se confunde por un método tenemos como
consecuencia la incapacidad de los usuarios de buscar otras soluciones
para su trabajo.

__Uno de los principales problemas actuales en la industria editorial
es que las metodologías más empleadas ya no ofrecen las soluciones
que los editores requieren.__

El mundo de la edición, por tradición, ha fijado sus procesos
de producción de libros a partir del diseño gráfico. En un contexto
digital esto implica el enfoque [+++WYSIWYG+++](https://es.wikipedia.org/wiki/WYSIWYG).
Los libros hechos con _software_, por lo general, se elaboran
a partir de una interfaz gráfica que en todo momento te permite
ver su resultado final, tal cual será enviado a la imprenta o
visto en algún _ereader_.

Esto no es un problema si la intención es solo producir un soporte
para una obra. El [_desktop publishing_](https://es.wikipedia.org/wiki/Autoedición)
obedece al enfoque metodológico desarrollado para la impresión,
el soporte más común en la edición. Este tipo de enfoque lo observamos
en programas como PageMaker, QuarkXPress e InDesign. Por este
motivo, la migración entre estos programas no ha representado
un gran problema para el editor. Aunque sí ha generado serias
dificultades en el mantenimiento de los archivos debido al uso
de formatos propietarios.

Sin embargo, __las necesidades actuales en la edición implican
la producción de al menos dos soportes: el impreso y el digital.__
Los procesos de publicación más comunes en la edición no pueden
resolver este problema porque sus supuestos metodológicos surgen
a partir de la necesidad de publicación de libros impresos.

La necesidad de un soporte digital ha generado una tensión porque
la industria editorial está en transición y en adopción de nuevas
metodologías multiformato. __Si como editor se te dificulta publicar
con alta calidad y en varios soportes, no es porque estás rezagado,
sino debido a que las metodologías empleadas no son las más indicadas.__

La libertad metodológica en la edición implica que los editores
puedan optar por otros enfoques que les permitan evadir los problemas
generados por las metodologías tradicionales de publicación.
No obstante, esto llama a ver las obras y los libros ya no solo
como productos visuales, sino como código.

Detrás de todas las interfaces gráficas como Microsoft Word o
InDesign, los archivos con los que se trabajan son estructuras
más apegadas a los lenguajes de programación y de marcado.

__La edición se hizo código__ y TeX es el mejor y el más viejo
ejemplo ---es un sistema digital de composición tipográfica que
surge desde finales de los ochenta---.

### 3. Libertad de formatos

Los usuarios de _software_ carecen de autonomía si los formatos
de sus archivos condicionan el uso del programa que prefieren.
Cuando un formato obliga al usuario a emplear un _software_ específico
lo que tenemos es una dependencia tecnológica hacia los desarrolladores
de este _software_.

En la industria editorial existe un consenso en que esto no es
un problema de primer orden. La dependencia a compañías de _software_
como Microsoft o Adobe, por lo general, se aplaude y se incentiva.

No obstante, esto quiere decir que el rumbo de una industria,
cuyo origen se remonta al siglo +++XVI+++, ahora depende de las
elecciones realizadas por la industria del desarrollo de _software_,
cuyo surgimiento fue en el siglo +++XX+++.

Antes de 1985 ---por poner una fecha: el desarrollo de PageMaker---
los monopolios en la industria editorial surgían de las tecnologías
creadas en su mismo ámbito como las máquinas de monotipo, linotipo
u _offset_. Con el surgimiento del _desktop publishing_ se comenzó
el traslado de la evolución técnica de los procesos editoriales
a manos de compañías desarrolladoras de _software_.

Si esto no parece problemático, solo tómese en cuenta que el
objetivo en el desarrollo propietario de _software_ es la generación
de ingresos a partir de la dependencia de los usuarios hacia
sus programas. __No es y nunca será una prioridad solventar los
problemas de la industria editorial, al menos que esta solución
pueda capitalizarse.__

¿Harto de las viudas, las huérfanas, los ríos y los callejones?
¿No se supone que son problemas que al parecer serían sencillos
de arreglar dado los avances tecnológicos? Sí, pero para los
desarrolladores de _software_ no se consideran dificultades prioritarias.
La mayor clientela para estas compañías son los editores angloparlantes
o germanos para los cuales este tipo de cuidado editorial no
es tan prioritario como lo es para los editores en español.

## El traslado a formatos abiertos

__Con el fin de tener mayor control y mejor mantenimiento de
los proyectos editoriales la aproximación más recomendable es
la adopción de estándares abiertos.__ Estos estándares implican
el uso de formatos abiertos con una nítida doble finalidad. Por
un lado, los formatos abiertos evitarán mayor pérdida de información
con el tiempo. Por el otro, estos formatos incrementan la soberanía
tecnológica de los editores al eludir que sus proyectos queden
sujetos al uso de programas específicos.

Si como editor tu flujo de trabajo implica el uso de Microsoft
Office y de Adobe InDesign, __lo recomendable es guardar los
archivos en formatos +++DOCX+++ y +++IDML+++. No obstante, no
es lo único que puedes hacer__.

Para las paqueterías de oficina existe la posibilidad de adoptar
estándares abiertos desarrollados por la comunidad de [The Document
Foundation](https://www.documentfoundation.org/) (+++TDF+++).
La diferencia entre los formatos abiertos ofrecidos por +++TDF+++
y Microsoft es que los estándares de +++TDF+++ surgen a partir
de consensos entre su comunidad ---de la que puedes formar parte---
y no a partir de las decisiones tomadas por los ejecutivos de
una compañía de _software_.

Entonces, si deseas un mayor nivel de autonomía, __en lugar de
guardar tus archivos en +++DOCX+++, puedes emplear el formato
+++ODT+++. Microsoft Office [permite esta posibilidad](https://support.office.com/en-us/article/Use-Word-to-open-or-save-a-document-in-the-OpenDocument-Text-odt-format-20E5189F-86F8-4D8F-AE74-EA06B7DF3B0E).
Como también puedes usar [LibreOffice](https://es.libreoffice.org/)
---la paquetería de oficina desarrollada por +++TDF+++--- para
abrir tus documentos en +++DOCX+++ u +++ODT+++.__

¿Qué alternativas se tienen para InDesign? Por desgracia, __aunque
el formato +++IDML+++ es abierto, aún no existe otro programa
parecido a InDesign con el que se pueda trabajar.__ [Scribus](https://www.scribus.net/)
es la alternativa libre más cercana al modo de trabajo de Adobe
y en el futuro tiene proyectado la posibilidad de trabajar con
archivos +++IDML+++.

Mientras tanto, la única forma de usar archivos de InDesign en
Scribus es mediante su [exportación a +++EPS+++](https://www.techwalla.com/articles/how-to-import-from-indesign-to-scribus).
Por desgracia, esto probablemente lleve a una pérdida de información,
por lo que varios editores no se sentirán satisfechos con esta
solución.

__Sin embargo, si como editor deseas probar con otras metodologías
distintas a Adobe, desde los ochenta se cuenta con TeX.__ Este
sistema de composición tipográfica es robusto y muy centrado
en obras con mucho peso textual. TeX y su formato abierto +++TEX+++
es muy usado en entornos académicos, pero su versatilidad permite
su empleo en obras literarias.

La cuestión con TeX es que su robustez lo hace muy complejo para
editores que vienen desde el enfoque metodológico de Adobe y,
en general, del diseño editorial. __Los libros hechos con TeX
no se diseñan, se compilan__. Los archivos +++TEX+++ implican
el uso del lenguaje de marcado y de programación de TeX.

Esto genera una larga curva de aprendizaje que desalienta la
implementación de TeX más allá de los círculos académicos. Sin
embargo, __¿qué pasaría si desde los procesos formativos los
editores aprendieran a trabajar con formatos abiertos y con distintos
enfoques metodológicos?__

Un archivo +++TEX+++ creado en los ochentas sin problemas puede
seguir utilizándose para nuevas reediciones o reimpresiones.
No hay pérdida de información. No hay necesidad de aprender nada
nuevo. No es menester la actualización de los archivos. Lo único
que TeX requiere es que el editor dedique su tiempo en aprenderlo.

En pocas palabras, ¿qué da mayor versatilidad, flexibilidad e
independencia en la edición: la constante migración y aprendizaje
entre programas y formatos propietarios o el perseverante aprendizaje
de un lenguaje y un formato abiertos, cuyo conocimiento no caduca,
solo se profundiza?

<aside class="espacio-arriba2">

###### Sobre esta entrada

* Título original: «La conveniencia de los formatos abiertos para la industria editorial».
* Título en el _blog_ de Mariana: «La conveniencia de los formatos abiertos para la industria editorial».
* Fecha de publicación: 27 de junio del 2019.
* [Redacción original](https://gitlab.com/NikaZhenya/entradas-eguaras/blob/master/29%20-%20La%20conveniencia%20de%20los%20formatos%20abiertos/entrada.md).
* [Revisión de Mariana](https://marianaeguaras.com/la-conveniencia-de-los-formatos-abiertos-para-la-industria-editorial/).

</aside>
