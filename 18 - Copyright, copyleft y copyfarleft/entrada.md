# *Copyright*, *copyleft* y *copyfarleft*

Junto con [lo gratuito, lo abierto y lo libre](https://marianaeguaras.com/lo-gratuito-lo-abierto-y-lo-libre-o-la-gratuidad-el-acceso-abierto-y-la-cultura-libre/)
acontece una serie de replanteamientos sobre el respaldo legal de cada
autor en relación con el modo en como difunde su obra. En varios de 
los casos esto se traduce a una serie de confusiones que vale la pena
abordar, aunque sea de una manera muy superficial.

## *Copyright*

El «derecho de copia» o también conocido como *copyright* es una 
doctrina jurídica que protege al autor y a su obra. Su símbolo es
mundialmente conocido (©) y su legislación parece extenderse incluso
hasta en contra de la voluntad del mismo autor.

Pero su origen es un tanto distinto. Con la expansión de la 
imprenta en Gran Bretaña, algunos autores empezaron a cuestionar el
monopolio que tenían varios editores/impresores —por comodidad se 
usará cada término indistintamente—. Con anterioridad no existía una 
legislación clara respecto a la relación entre ellos por lo que en 
1710 la Reina Ana promulgó un [estatuto](https://es.wikipedia.org/wiki/Estatuto_de_la_Reina_Ana) 
que daba plenos derechos a los autores de explotar sus obras.

Este estatuto les daba «derechos de copia» a los impresores, haciendo
explícito que no eran los propietarios de la obra. **Este derecho de
explotación exclusiva de los editores solo es por un tiempo 
limitado**. Una vez caducado el lapso, el autor era libre de dar 
prórroga al contrato o buscarse un nuevo impresor. En caso de 
fallecimiento del autor, una vez cumplido el lapso, la obra pasaba al 
[dominio público](https://es.wikipedia.org/wiki/Dominio_p%C3%BAblico).

**El lapso inicial era de tan solo catorce años** y durante un tiempo
permitió una gran flexibilidad y expansión de la industria editorial.
Pero también dio hincapié a nuevos conflictos: por un lado el interés
del editor de ampliar el plazo de sus derechos de explotación; por el
otro, la preocupación de autores y familiares de no poder lucrar más
con la obra una vez fallecido el autor.

Ante estos conflictos, poco a poco las leyes de *copyright* se fueron
modificando para incluir la posibilidad de herederos a las obras y
así continuar su explotación comercial, o la ampliación de los plazos
de explotación. En el panorama actual, el titular de los derechos 
puede ser cualquier persona o institución —pública o privada— que el 
autor señale y sus derechos de explotación pueden extenderse a por lo 
menos cincuenta años después de la muerte del autor, aunque la mayoría 
de los Estados es ya de setenta años —como el caso de la Unión Europea 
y Estados Unidos— o incluso de cien años —como sucede en México—.

Si bien esto ha permitido ampliar los plazos para la adquisición de 
regalías, también ha generado el problema de las obras huérfanas 
—trabajos que aún tienen *copyright* pero cuyo titular de derechos
no puede ser localizado, por lo que la obra en teoría no puede ser
utilizada— así como la imposibilidad de emplear un material sin la
autorización del titular —lo que supone que cualquier uso no 
autorizado, aunque sea de elogio, está infringiendo con los derecho de
autor—.

## *Copyright* y derecho de autor

El «derecho de copia» surge en Gran Bretaña al comienzo de la 
Ilustración, pero como es evidente, no toda legislación moderna viene 
del mundo anglosajón. Así como John Locke es uno de los «padres» de
esta tradición jurídica, en la Europa continental surge otra 
tradición: el derecho de autor.

Los «padres» del derecho de autor son Kant, Hegel y varios juristas
franceses. Este derecho tiene la similitud de que surge para proteger
al autor y su obra pero con la diferencia de que en realidad de trata
de dos tipos de derechos: los patrimoniales y los morales.

**Los derechos patrimoniales en sus efectos son iguales al 
*copyright***, son los derechos que permiten transmitir las 
posibilidades de explotación de una obra. Esto trae implícito que la
obra se considera un «fruto del trabajo» del autor, el cual puede 
decidir la manera más oportuna de sacar «beneficios por su labor».

Sin embargo, autores como Kant y Hegel consideran que la obra es más 
que eso: una creación es una expresión de la personalidad o la 
voluntad del creador. La relación entre el autor y su obra no es 
«accidental» —el trabajo no es sustancial al autor— sino intrínseca y 
única —la relación entre el autor y *su* obra es cualitativamente 
distinta a cualquier relación que esta obra pudiera tener con otra 
persona—. Es decir, al momento de producir una obra, no solo se *crea 
un producto* mediante el trabajo, sino que también acontece una 
intimidad por la cual es posible identificar que una obra *forma 
parte* del autor.

Los derechos de patrimoniales se quedan cortos al proteger a la obra
desde esta perspectiva, ya que solo la tratan como un producto. Por
ello, **los derechos morales funcionan para reconocer la *paternidad*
de una obra y el derecho al autor a proteger su integridad**. Dado a 
que los derechos morales tratan a la obra como extensión del autor,
estos son de carácter intransferible e inalienables —o eso se supone—.

Con esto, los derechos de autor son muy similares al *copyright*, por
lo que acarrean las mismas problemáticas que plantea el «derecho de
copia». Esto también permite que sea común utilizar el símbolo «©» 
más la añadidura de un «D. R.», explicitando que los «derechos 
reservados» engloban tanto los derechos patrimonial y morales. Al 
mismo tiempo abre paso a ciertas ambigüedades ya que más allá de un 
ámbito romántico o «metafísico», la supuesta relación intrínseca e 
íntima entre el autor y su obra no puede explicarse del todo, pese a 
que en un primer momento parezca evidente.

## *Copyleft*

Hay varias personas que por motivos económicos, políticos o sociales
no están de acuerdo con las legislaciones actuales en torno al derecho
de autor. Uno de los principales argumentos es que las leyes actuales
restringen el flujo creativo que durante mucho tiempo se dio en el
quehacer cultural en pos de casos minoritarios de infracción.

Como propuesta surge el movimiento del *copyleft*, cuyo nombre es un
juego de palabras por el que se manifiesta que es algo opuesto al
*copyright*. Su símbolo —cada vez más popular— es la inversión del
símbolo del *copyright*. Al no existir aún ese carácter en la mayoría
de las fuentes tipográficas, como alternativa en texto plano se 
utiliza una «c» invertida y entre paréntesis: «(ɔ)».

El origen del *copyleft* se remonta a los años ochenta del milenio
pasado en el ámbito del *software*. Richard Stallman y el movimiento 
del *software* libre fueron los primeros en buscar alternativas 
*jurídicas* que protegieran el código para asegurar su libre 
distribución. **Por primera vez la historia los autores empezaron a
preocuparse por tener un respaldo legal que les permitiera distribuir
su obra libremente**. Es así como surgió la [Licencia Pública General GNU](https://es.wikipedia.org/wiki/GNU_General_Public_License)
o [GPL]{.versalita} por su acrónimo en inglés.

A finales de los noventa, estas ideas en torno a la propiedad del
código y del *software* se expandieron a los productos culturales en 
general. Su principal representante es Lawrence Lessig y el 
movimiento de la cultura libre. Las licencias [Creative Commons](https://es.wikipedia.org/wiki/Creative_Commons) 
surgen a partir de este movimiento hasta el punto que en la actualidad 
son las licencias *copyleft* más populares.

En general lo que estas licencias promulgan es un paso del «todo» a
«algunos» derechos reservados. Pero más relevante aún, **de manera 
implícita da a entender que la obra es tanto un producto individual 
como social**: un bien común. El objetivo es simplificar el aparato
jurídico y burocrático para que cualquier persona —según los términos
de cada licencia— puedan utilizar un producto cultural como «suyo». La 
meta es constituir un ecosistema cultural por el cual usuarios y 
creadores no se vean limitados, al mismo tiempo que asegura la 
integridad de la creación y su creador.

Estos ideales poco a poco han tenido mayor recepción pero también
ha provocado algunos conflictos. Por un lado, se consideran una 
amenaza directa a la forma en como se ha gestado la cultura desde la
Ilustración —aunque en realidad solo ataca de manera directa los
intereses monopólicos de algunas compañías o asociaciones, tal como
el *copyright* surgió para eliminar el monopolio de editores e 
impresores—. Por el otro, crea un nuevo ecosistema cultural el cual
se considera que debería de ser más regulado por los Estados.

## *Copyleft* y derecho de autor

El problema de la regulación afecta el ámbito legislativo ya que 
ninguna tradición jurídica —anglosajona o continental— había 
considerado la posibilidad de que el autor quisiera proteger 
«libremente» su obra. De hecho, la «libertad» del autor se había 
comprendido como el respecto y protección de su obra como su 
«propiedad» y las «libertades» económicas y de comercio que de esto se
desprendía.

Esta otra noción de lo «libre» —donde la obra es un «bien común» y no
una «propiedad»— y la demanda de los ciudadanos en su respaldo 
jurídico ha inducido a la modificación de las distintas legislaciones. 
En sentido jurídico, **Las propuestas de licenciamiento del *copyleft* 
no son una sustitución del derecho de autor**, sino un «suplemento» 
por el cual se constituyen como licencias de uso no exclusivas. 

Es decir, los derechos de explotación exclusiva de una obra no son 
transferidos ni la obra pasa a un dominio público voluntario, sino que 
siguen perteneciendo al autor o sus herederos. Esto no sería 
conflictivo si de manera efectiva se respetara la voluntad del autor
por dejar «abierta» su obra aun después de su muerte, pero no siempre
se da el caso, como ya se mencionó en [«Derecho de autor cero»](https://marianaeguaras.com/derecho-de-autor-cero/).

Esto hace que, pese a las intenciones del movimiento del *copyleft*,
en la actualidad distintas legislaciones interpreten sus licencias
como una extensión a los derechos de autor. Idea que parece un tanto
tergiversación, pero que quizá no lo sea tanto…

## *Copyfarleft*

Existe el derecho de autor y el carácter opcional de permitir el uso
«libre» de su obra, ¿qué problema podría haber en todo esto? Para 
algunos críticos, esta «apertura» en el uso de la obra no toma en 
cuenta factores políticos y económicos que afectan directamente el
bienestar de un autor: ¿qué pasa si una obra con *copyleft* es 
explotada de tal manera que un tercero obtiene un beneficio económico
que no es compartido con el autor?

Supongamos que una artista *x* decide publicar su obra *y* con 
licencia [[CC-BY]{.versalita}](https://creativecommons.org/licenses/by/2.0/deed.es) 
—solo pide atribución a la obra—. A continuación una empresa *z* ve un 
gran potencial comercial de *y*, por lo que comienza con su 
explotación. Por último, resulta que esta explotación ha dado grandes 
beneficios económicos a *z* y, dado al tipo de licencia, esta no está 
comprometida a pagarle regalías a *x*, acumulando así todos los 
beneficios.

Las palabras clave aquí son «explotación» y «acumulación»: para estos
críticos **si bien el *copyleft* intrínsecamente pretende otro medio
de compartir y crear cultura, no implica la eliminación de la
explotación de los artistas por parte de un sistema económico de
acumulación de capital**. Es decir, el *copyleft* se queda corto al
no querer ir más allá de la manera en como se relacionan autores y
consumidores. Peor aún, prolonga la idea capitalista del «libre 
mercado» al buscar disminuir o simplificar las funciones tradicionales
de los Estados —como la de ser un regulador entre el autor y su 
público o las entidades que explotan su obra—.

Esto genera un problema ético de fondo: ¿*debe* la compañía *z* 
repartir el capital acumulado a la artista *x*? Jurídicamente no tiene
el *deber* de hacerlo, pero desde un enfoque ético *debería* de 
llevarlo a cabo, de lo contrario, estaría prolongando la cadena de 
explotación del «obrero/autor» por parte del «capitalista». ¿Qué se 
podría pensar si la artista *x* no tiene otro medio de subsistencia y 
al mismo tiempo la compañía *z* está lucrando de manera constante con 
su obra *y*? ¿Acaso no es la prolongación de una situación injusta?

Para evitar este problema, surge la iniciativa del *copyfarleft* el
cual es también un juego de palabras que implica ir aún más a la
«izquierda» del espectro político del *copyleft*. La principal 
característica de los tipos de licencia propuestos por esta 
iniciativa es la adición del requisito «no-capitalista».

La [Licencia de Producción de Pares](https://endefensadelsl.org/ppl_deed_es.html)
sintetiza muy bien esta singularidad:

> No Capitalista - La explotación comercial de esta obra solo está 
> permitida a cooperativas, organizaciones y colectivos sin fines de 
> lucro, a organizaciones de trabajadores autogestionados, y donde no 
> existan relaciones de explotación. Todo excedente o plusvalía 
> obtenidos por el ejercicio de los derechos concedidos por esta 
> Licencia sobre la Obra deben ser distribuidos por y entre los 
> trabajadores.

¿Qué problema puede tener este tipo de licencias? En un comienzo los 
prejuicios hacia su propio discurso: son pocas las personas que tienen 
empatía ante críticas socialistas, marxistas o anarquistas, más aún
en un ecosistema cultural definido a partir de subsidios estatales, 
«empresas culturales» o el «emprendimiento individual».

## *Copyfarleft* y derecho de autor

Como licencia de uso, el *copyfarleft* es tratado como un tipo de
licencia *copyleft*. Sin embargo, el apego a su criterio 
no-capitalista puede no ser claro para la mayoría de las 
legislaciones: ¿qué se entiende por «sin fines de lucro», a cualquier
tipo de colectivo o solo a las organizaciones no gubernamentales sin
fines de lucro debidamente registradas?, ¿qué se entiende por 
«autogestión»?, ¿qué se interpreta por «explotación»?, etcétera…

No existe certeza jurídica sobre el respeto a este tipo de licencias; 
sin embargo, el meollo no es de índole legislativa, sino política. 
**El *copyfarleft* tiene el interés de hacer explícito el espectro 
político del autor y su obra, y el seguimiento a su principio
no-capitalista es, ante todo, un acto ético y de respeto a estos 
principios**…

Y tú, ¿qué tanto te vas a la «derecha» o a la «izquierda»? ¿Son estas
tres tendencias hacia los derechos de autor completamente disímiles?

