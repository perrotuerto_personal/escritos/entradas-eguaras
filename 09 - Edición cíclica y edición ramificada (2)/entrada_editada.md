# 9. Edición cíclica y edición ramificada: del surgimiento al tronco y las ramas

## Surgimiento de una idea

¿Y si cambiamos de perspectiva? ¿Si en lugar de hablar sobre la
diversidad en los formatos finales, pensamos en un método multilateral?
La idea es sencilla: **no nos concentremos en los formatos, sino en los
caminos que llevan a ellos**. El supuesto también es evidente: a
múltiples formatos, diversos senderos.

Pese a que la idea es sencilla, en el grueso editorial se interpretará
como un discurso que apuesta por la pretendida destrucción de su
tradición, cuando en realidad atenta a un ideal metodológico, no a los
conocimientos que a lo largo de siglos se han ido acumulando para
publicar «buenos» libros.

Una prueba de esta extrapolación es patente cuando se analiza la
tradición editorial: las técnicas han variado a través de los siglos,
pero la idea de ir a un punto +++A+++ a uno +++B+++ ha permanecido.

En la edición tradicional el punto +++A+++ era igual al texto original 
y el punto +++B+++ igual al impreso. En la edición cíclica 
+++A+++ permanece, mientras que +++B+++ es el fin de todos los 
ciclos, obteniéndose así no solo el impreso, sino también al menos un formato 
digital.

Sin embargo, en la **edición ramificada** no hay una consecución de 
+++A+++ a +++B+++, sino **un tronco (+++A+++) con 
diversas ramas (+++B+++, +++C+++, +++D+++…) que cesan 
su crecimiento, otras se desprenden y unas más se convierten en formato para 
más ramas**.

![Esquema comparativo entre la edición cíclica y la ramificada.](../img/img_08-01.jpg)

Esta concepción metodológica surgió de un campo especializado: la
elaboración de documentación de _software_. En el surgimiento de la era
digital pronto se vio la necesidad de textos que explicaran el uso del
_software_. Sin embargo, debido a las distintas preferencias de consulta
y de _hardware_, se hizo menester crear la documentación en distintos
formatos.

Pronto se percibió la dificultad presente en la edición cíclica: cada
nuevo soporte implica un aumento en la inversión de tiempo y de
cuidados, y por ende, de recursos. Para combatir este problema y ahorrar
en material impreso, a partir de los noventa surge la idea del [_single
source publishing_](https://en.wikipedia.org/wiki/Single_source_publishing)
(+++SSP+++), cuya característica es el desarrollo multilateral de
diversos formatos.

Aunque la idea tiene poco más de veinte años, es ahora cuando empieza a
expandirse para el resto del quehacer editorial. En la actualidad, la
edición ya no solo es digital, sino también la publicación. **Los libros
electrónicos han puesto en el centro de la discusión el problema
metodológico que el sector editorial viene arrastrando desde el inicio
de la «revolución» digital**.

## El tronco y las ramas

Pero, en concreto, ¿qué es la edición ramificada? Lo primero que podría
entenderse es que significa una apertura en todo sentido. No obstante,
el término «_single source_» ayuda a esclarecer el **primer elemento**
de esta metodología de trabajo: **la edición empieza con un «archivo
madre»**.

El «archivo madre» es un documento a partir del cual se crean el resto
de los formatos: es el tronco. Este archivo puede ser tanto el texto
original del autor como el documento editado, ya que el elemento mínimo
necesario es que esté en un [lenguaje de
marcado](@index[5]).

El archivo madre obedece a una dimensión estructural a partir de un
conjunto de etiquetas que indican cada elemento del texto (véase
[aquí](@index[7])
para más información). La edición del texto puede darse antes o después
de esta estructuración.

Lo recomendado es que la edición y la estructuración se den en conjunto,
porque antes de la publicación quien edita es uno de los principales
conocedores de la obra. La desventaja es que se requiere saber al menos
un lenguaje de marcado, pero esto se solventa si se recurre a un
lenguaje de marcas ligero.

Los lenguajes de marcas ligeros son cómodos de leer, fáciles de escribir
y sencillos de convertir. Por este motivo, es preferible un lenguaje
«ligero», como Markdown, que uno «completo», como +++XML+++,
+++HTML+++ o TeX.

El **segundo elemento** de este método es ir de lo simple a lo complejo.
Cada formato presenta sus propias particularidades. En el impreso se
requieren ajustes manuales por cuestiones ortotipográficas o de diseño;
en el +++EPUB+++ a veces es preciso ordenar gráficas o tablas para una
mejor visualización; en la lectura _online_ sin inconvenientes se saca
provecho de la [visualización interactiva](https://d3js.org/) de
información, etcétera. Para evitar la herencia de características, como
sucede en la metodología cíclica, la edición ramificada parte de un
documento con los elementos comunes, para después hacer ajustes según el
caso.

El traslado del archivo madre a cada uno de los formatos finales exige,
en la mayoría de los casos, el uso de un nuevo lenguaje. Si se parte de
Markdown, se requiere +++XHTML+++ para un +++EPUB+++,
+++XML+++ para InDesign o TeX para LaTeX o ConTeXt. Si bien se
recomienda el conocimiento de estos lenguajes, el **tercer elemento** es el uso
de conversores para el ahorro de tiempo.

El trabajo con un lenguaje de marcado posibilita la automatización de la
traducción a otros lenguajes. Sin embargo, la traducción realizada por
una máquina, por lo general, necesita modificaciones y cotejos. Los
conversores no son una solución final, pero evitan el trabajo monótono.

El mejor _software_ que puede ayudarnos en esta tarea es
**[Pandoc](http://pandoc.org/)**. Este programa libre es de los más
poderosos que podemos encontrar. Y, si bien en algunos casos el
resultado no es el esperado, el tiempo involucrado en los ajustes es
menor a volver a formatear el documento.

Esta inversión de tiempo permite diferenciar la edición ramificada de
muchos _software_ milagro que se ofrecen en el mercado. Esta clase de
programas se autoperciben como la «solución» a varios problemas que el
editor encuentra al momento de publicar en múltiples soportes.

La edición ramificada no es un _software_ ni un lenguaje, mucho menos
una solución y tampoco un entorno de trabajo ---guiño a Adobe---. **La
edición ramificada es una metodología que puede manifestarse de
múltiples maneras**, unas más acabadas que otras. Al ser un método
también cuenta con la posibilidad de pulirse o de descartarse si crea
inconvenientes.

Por ello, el **cuarto elemento** es que la edición ramificada solo está
pensada cuando existe la posibilidad de múltiples formatos. Si la obra
se proyecta como un «libro objeto» o una publicación artesanal basada en
métodos analógicos este método no tiene cabida.

Los conversores no son lo único que permiten el ahorro de tiempo. El
**quinto elemento** consiste en que el tamaño del equipo de trabajo es
proporcional a la agilización y división del trabajo. Una diferencia
nítida entre la edición cíclica y la ramificada es que en la última
ningún formato final parte de otro, lo que permite el trabajo paralelo
en la producción de cada formato.

Cada soporte emprende su camino de manera simultánea a partir del
archivo madre, resaltando el **sexto elemento** metodológico: la edición
ramificada es edición sincrónica, independiente y descentralizada.

Uno de los temores que surgen en la edición ramificada es que la
simultaneidad y autosuficiencia puede dar cabida a una divergencia en el
contenido, ya que no existe un mecanismo centralizado para el control de
la edición. No obstante, en muchos casos la edición cíclica implica una
gran pérdida de control, debido a que el encargado del cuidado editorial
tiende a desconocer los procesos «periféricos» y «adicionales» a la
producción del soporte impreso.

Si hablamos de «control», en el contexto digital **el cuidado de una
obra no solo debe de ser editorial, sino también técnico**. Si no hay
dominio sobre el código no existe la seguridad de que el texto, aún con
lenguaje de marcado, sea fácil de convertir o sencillo de leer y
analizar.

La pérdida de control puede resolverse de tres maneras distintas:

1. Realizar la edición y las estructura de la obra al mismo tiempo,
   llevando a cabo ambos procesos de manera simultánea.
2. Borrar el archivo creado y volver a generarlo a partir del archivo
   madre; la vía más cómoda pero que supone volver a pulir los detalles
   derivados de la conversión.
3. Crear un programa para que analice cada archivo de cada formato y lo
   compare con el contenido del archivo madre. De este modo, si en un
   formato A la palabra «andará» accidentalmente se cambia por
   «andara», el _software_ lanzaría una advertencia de que en el
   archivo madre esa palabra es «andará». Esto permitiría encontrar
   modificaciones accidentales del texto cuando se esté modificando la
   estructura o el diseño de manera manual. Y sería solo una
   advertencia, ya que el cambio podría ser intencional, como
   «murcié\\-lago» en TeX a comparación de «murciélago» en el madre,
   solo para sugerir una separación silábica para arreglar una caja.
   Para esto es necesario obtener las palabras del texto, ignorando la
   sintaxis de cada lenguaje de marcado.

No obstante, en más de una ocasión existen correcciones de último momento
que también han de añadirse a cada uno de los formatos. La corrección
manual involucra más cotejos e incluso abre la puerta a más erratas. El
**séptimo elemento** llama al [uso de
diccionarios](https://marianaeguaras.com/como-usar-el-diccionario-del-usuario-en-indesign-para-evitar-errores-de-maquetacion/)
y de expresiones regulares
([_regex_](https://es.wikipedia.org/wiki/Expresión_regular)) para evitar
o automatizar las modificaciones. El diccionario permite detectar
erratas en el archivo madre. El uso de _regex_ facilita corregir los
archivos sin la necesidad de ir caso por caso.

El uso del diccionario es recomendable únicamente para el cotejo de
posibles erratas, sea caso por caso o mediante la creación de una lista
ordenada con las palabras dudosas. En cuanto a _regex_, hay que tener
cautela con su uso. El dominio de las expresiones regulares se adquiere
con el tiempo, por lo que las primeras implementaciones tienen que ser
básicas y después de hacer sido aplicadas en pruebas.

Continuando con la idea de un _software_ que monitoree las ramas, este
también tiene que permitir la corrección automatizada. El procedimiento
sería la realización de una corrección manual en el archivo madre que,
de manera automática, el programa traslade los mismos cambios a cada
formato.

<aside class="espacio-arriba2">

###### Sobre esta entrada

* Título original: «Edición cíclica y edición ramificada (2)».
* Título en el _blog_ de Mariana: «Edición cíclica y edición ramificada: del surgimiento al tronco y las ramas (2/3)».
* Fecha de publicación: 11 de julio del 2017.
* [Redacción original](https://gitlab.com/NikaZhenya/entradas-eguaras/blob/master/09%20-%20Edici%C3%B3n%20c%C3%ADclica%20y%20edici%C3%B3n%20ramificada%20(2)/entrada.md).
* [Revisión de Mariana](https://marianaeguaras.com/edicion-ciclica-y-edicion-ramificada-del-surgimiento-al-tronco-y-las-ramas/).

</aside>
