# Dolores de cabeza frecuentes para un editor de _ebooks_

Al momento de desarrollar un libro electrónico hay varias elementos
que pueden fallar. Lo primero que sale a flote es la confianza sobre
la capacidad de quien hace el libro. Sin embargo, en varias ocasiones
el _ebook_ no se muestra como debería por cuestiones ajenas a la
calidad técnica del editor.

## 1. Una analogía (errónea): la página

En la edición de libros impresos la página es el paradigma para la 
lectura y disfrute de una obra. Tras bambalinas, la página también
es el modelo para la corrección de errores.

¿Cuántas veces no hemos leído, «Te mando los siguientes cambios: en 
_la página 5_…»? La página es muy útil para moverse a través de la obra. 
Más cuando no se lee la obra, sino que se emplea como referencia. 

Un tipo útil de referencia es la que indica donde hay erratas, errores 
de formación y más. Esto bajo el supuesto de que la página tiene una
dimensión constante en todos los dispositivos de lectura. 

Para desgracia del control editorial, en las publicaciones electrónicas
la página nunca tiene el mismo tamaño. La página como un lienzo o un
contenedor no es una analogía lo suficientemente fuerte para representar
el rol que desempeña en la edición.

La página implica una idea de un orden perenne. Mientras que el libro
sea libro, la página siempre permanecerá igual, se raye, se moje o se
rompa.

En un libro electrónico este carácter fijo ya no existe. La «página»
digital es solo una remembranza del papel que desempeñaba durante la
lectura. La tradición editorial trasladó la publicación como pergamino
a una conformada por varias páginas. 

En la edición digital la «página» es solo una convención que da al 
lector el mismo tipo de dirección al que está acostumbrado: izquierda 
/ derecha → arriba / abajo → anverso / reverso.

Lo más grave que este equívoco no es la dificultad añadida para poder 
localizar las referencias a errores. ¿Qué hay de las viudas y huérfanas, 
de la imagen y su pie de foto anclado, de los callejones y desbordamientos?

En la publicación digital, al ser variable el tamaño de la «página»,
no se puede dar solución definitiva a estos problemas en la formación.
Lo que funcione para un tamaño de pantalla o dispositivo no necesariamente
servirá para otros tipos de dispositivos. Incluso puede ir en detrimento
de la experiencia de lectura, como son indeseables saltos de línea o
desbordamientos.

La cuestión reside en que los recursos necesarios para solventarlos no 
los justifican: el lector promedio es ciego a estos errores de formación.
Esto asimismo quiere decir que estamos ante dos tensiones en el
constante devenir del desarrollo de _ereaders_. 

Por un lado lo que el programador y el editor consideran relevantes. 
En no pocas ocasiones quien programa tiene otras prioridades: concibe 
al libro de modo distinto a quien lo edita. 

Por el otro, no puede dejarse de lado que el principal desarrollo de
los _ereaders_ se da en el mundo anglosajón. Las prioridades en el cuidado
editorial en inglés difieren con las de nuestra lengua. Ejemplo: 
la levedad con la que se permiten las viudas y las huérfanas.

Cuando nos adentramos al cuidado editorial de publicaciones electrónicas
en español no queda sino aceptarlo. La página y todo su cuidado en
la formación en nuestra lengua queda muy mermada cuando hablamos de
«páginas» en los _ereaders_.

¿Si en lugar de rendirnos decidimos conscientemente comenzar el
desarrollo de estándares en _ereader_ amigables a la tradición editorial
en español?

## 2. El culpable directo: el editor-diseñador-desarrollador

Una vez aceptada a regañadientes la limitación que nos da la «página»
del _ebook_, acontecen otros problemas. Estos en principio son achacables 
a la persona que desarrolla la publicación.

La falta de profesionalización en la creación de publicaciones electrónicas
provocan que muchas veces la calidad posible no sea alcanzada. Entre
el cuidado del texto como contenido, luego como diseño y hojas +++CSS+++,
hasta como estructura y etiquetas +++HTML+++ acontece una constante
transposición de dimensiones del mismo texto.

No es sencillo desarrollar el ojo para tener capacidad de percibir al
texto en estas tres dimensiones al unísono. Lo que parece una tarea
sencilla, se convierte en un infierno para quien tiene un primer
acercamiento a la publicación digital.

Como lectores o personas ajenas a los procesos requeridos para hacer un
_ebook_ es sencillo juzgar un libro por lo que se ve. Como es común, 
un libro electrónico tiende a tener una formación más simple así como es 
estéticamente menos placentero. De ahí se concluye que su creación ha 
de ser sencilla.

Nada más descontextualizado que esta suposición. Si el _ebook_ tiende
a ser «feo» o «simplón» no es porque el formato no permita más, sino
debido a que su dominio exige un largo camino para quien aprendió a
editar a través del sendero visual del diseño editorial.

El formato estándar del libro electrónico permite muchas posibilidades
interesantes. Sin embargo, el desarrollador de _ebook_ no solo tiene
que aprender esto, sino también a repensar el diseño a través de hojas
de estilo y a mantener la calidad editorial en un contexto de «páginas
fluidas».

A esto se suma que el trabajo realizado en una publicación electrónica
por lo regular es menospreciado por quienes se dedican a la publicación
de impresos. Como resultado tenemos que muchas veces, aunque ya se
cuente con la capacidad de mejorar el _ebook_, no se lleva a cabo debido
a que el tiempo requerido y el poco reconocimiento no lo justifica.

¿Si ya por fin nos libramos del menosprecio hacia el _ebook_ y
en su lugar vemos el carácter tripartita de su gestación y la necesidad
de hacerle frente?

## 3. El diligente (y menospreciado): el +++EPUB+++

Entre el resto de los formatos digitales el +++EPUB+++ es un estándar
en la industria con mucha potencia. Sus aspectos más conocidos es la
posibilidad de embeber objetos multimedia.

Pero otra característica relevante es la capacidad de poder incluir
JavaScript. Con este lenguaje de programación se permite añadir diversas
funcionalidades a nuestro libro. Ejemplos: animaciones, formularios,
modificación de estructuras y hasta videojuegos.

A esto sumamos el soporte para personas con deficiencia visual, a la 
creación de fórmulas matemáticas e incluso la experimentación para la
adición amena de partituras. Con las hojas aurales es posible escuchar
la obra de manera más natural. Las fórmulas matemáticas ya no tendrán
que ser imágenes, sino texto. Del mismo modo, las partituras podrían
ser seleccionables nota por nota y hasta escucharla con reproductores
+++MIDI+++.

Y no lo es todo, su estandarización da pauta para que todas estas
posibilidades puedan verse igual y del mismo modo en cualquier _ereader_.
Aquí nos aproximamos a terreno escabroso.

¿Si antes de seguir nos detenemos un poco y nos preocupamos por 
tratar de contemplar todo lo que el +++EPUB+++ tiene por ofrecer, en 
lugar de desaprobarlo y aclamar de necesidad de nuevos formatos 
(propietarios)?

## 4. Primeros cómplices: los renderizadores

El +++EPUB+++ tiene mucho por ofrecer, pero sus posibilidades por lo
general se han mermado por los renderizadores. Todo dispositivo o
programa que se utilice para leer un _ebook_ es un renderizador.

De las instrucciones y funciones incluidas en un +++EPUB+++, el _ereader_
o el _software_ procede a pasar de ese código a un resultado legible.
Esto es el proceso de renderización: la generación de una imagen a
partir de un conjunto de comandos.

Por desgracia, aunque existen estándares sobre cómo debería renderizarse
el _ebook_, por limitaciones de _hardware_ o decisión del desarrollador,
esto queda como sugerencia. Su efecto ya es muy conocido para muchos de 
nosotros: el libro nunca se ve de la misma manera. Ejemplos: ausencia 
de versalitas, falta de corte silábico o diferencia en el interlineado 
o los márgenes.

Un dispositivo antiguo no podrá dar el mismo desempeño al mostrar
las características más complejas e incluso simplemente no las ejecutará.
También existen otros casos en que desarrolladores como Apple o Amazon
restringen ciertas posibilidades. Ejemplos: Amazon no permite la adición
de ninguna funcionalidad de JavaScript y el soporte de +++CSS+++ aunque
extenso, tiene sus limitantes; Apple bloquea el uso de formularios que
[ni con JavaScript pueden eludirse](http://epubsecrets.com/what-ibooks-does-behind-the-scenes.php).

Esta disparidad en el soporte en no pocas situaciones provoca una tensión
entre quien desarrolla el _ebook_ y su cliente. En más de una ocasión
tanto el cliente como yo hemos tenido que ser lo suficiente pacientes
para demostrar que la ausencia de una característica no es por la mala 
calidad del código sino una cuestión del renderizador.

¿Si de una buena vez caemos en cuenta que las posibilidades del
_ebook_ están constreñidas a las características de cada uno de los 
dispositivos?

## 5. Segundos cómplices: los distribuidores

El _ebook_ puede ya estar desarrollado para sacarle su mejor partida
en una amplia de dispositivos. Sin embargo, aún así nos topamos con
dos grandes inconvenientes de los distribuidores.

El primero es que algunos de ellos —guiño de nuevo a Amazon y Apple—
no ponen a la venta el formato estándar que hemos trabajado. En su lugar,
los convierten en sus formatos propietarios.

La argumentación es que sus propios formatos están optimizados para sus
renderizadores. Por ello, lo aconsejable es utilizar sus herramientas 
de creación de _ebook_ para producir sus formatos de manera directa.

Además de que sus formatos propietarios son herederos directos de 
estándares abiertos, estos incluyen características adicionales que
habilitan el +++DRM+++ y la minería de datos. Súmese a eso el aumento
en las horas de trabajo para poder crear una publicación en distintos
formatos si se opta por la vía de la bifurcación del proyecto en lugar
de la conversión.

El candado digital que evita la reproducción no autorizada de una obra
(el +++DRM+++) es, en mi opinión, una de las cuestiones en las que la
edición digital ha despilfarrado una gran cantidad de recursos. Este
candado nunca se ha mostrado lo suficientemente seguro para las personas
que saben cómo eliminarlos.

El aprendizaje implicado tampoco es complejo. Desde [un uso sencillo de la terminal](https://github.com/apprenticeharper/DeDRM_tools)
hasta [un par de _plugins_ de Calibre](https://github.com/apprenticeharper/DeDRM_tools/tree/master/Obok_calibre_plugin)
es posible «liberar» las obras. 

En lo personal es un procedimiento común cuando adquiero algún libro
en Amazon o en sitios que cuentan con +++DRM+++ de Adobe. No por la
supuesta «piratería» que pretende evitar sino porque me desagrada la 
idea de que a pesar de haber comprado el _ebook_ su uso es limitado. 
Este desagrado muta en necesidad cuando todas estas tecnologías 
propietarias no dan soporte a sistemas operativos abiertos.

La minería de datos es una cuestión muy delicada y que casi no se ha
comentado en discusiones relativas a la edición y la publicación digitales.
Bajo el eufemismo de «análisis de hábitos de lectura» lo que varios
renderizadores llevan a cabo es una recolección de información que
posteriormente será procesada por sus compañías desarrolladoras.

Con todas las relaciones encontradas es posible monetizar con la manera
en como lees tus libros. ¿Maravillado porque Google, Amazon o Apple
siempre te sugiere buenos libros o de las coincidencias entre lo que
lees y publicidad que te muestra? No es fortuito, ellos buscarán el
medio para venderte algo, incluso a través de lo que lees.

El segundo inconveniente no es del lado del usuario hecho lector sino
como editor. Para quienes suben libros a distintas plataformas, en más
de una ocasión es un martirio que la publicación sea aprobada. Ejemplos:
Apple solo permite la subida de libros a través de iTunes Producer que,
como pueden adivinar, solo está disponible para macOS; Amazon ofrece
mucho menos regalías cuando el _ebook_ no es exclusivo de su plataforma.

Las soluciones me parecen limitadas. Una consiste en pedir prestada una 
Mac para subir el libro. La otra es que, como Amazon es dueño de su 
plataforma, uno elije si aceptar sus condiciones.

Como usuario y como editor no deberíamos estar constreñidos al uso de
_software_ específico para el desarrollo y comercialización de nuestros
libros. Tampoco tendríamos que ver mermadas nuestras posibilidades de
venta o de regalías según las condiciones de cada distribuidor.

A esto sumamos la ausencia de estándares en la distribución de los
_ebooks_. No es fortuito ni inevitable que se tengan que hacer 
procedimientos semejantes una y otra vez según la plataforma. 

En realidad todas las plataformas necesitan la misma información para 
poder colocar a la venta el libro. Si estas optaran directamente por
una publicación estándar, como el +++EPUB+++, también se haría posible
estandarizar su distribución.

La idea es sencilla. Cada distribuidor requiere acceso al _ebook_, a
la información para su venta y hasta algunas impresiones de pantalla.
La clase de archivo que utiliza iTunes Producer para procesar toda
esta información es una carpeta hecha pasar por archivo que contiene
entre las capturas de pantalla y el _ebook_ un +++XML+++ con toda la
información necesaria para la venta.

Podría encontrarse un esquema +++XML+++ estandarizado para esta información 
y a partir de ahí juntarlo con el libro y sus capturas de pantalla en
una carpeta comprimida como +++ZIP++. Con los accesos necesarios incluso 
se podría automatizar su subida mediante ++FTP++ o [`scp`](https://www.garron.me/es/articulos/scp.html).

Con esto se abriría la posibilidad de poder automatizar la distribución
y así tener ya pleno control de todos los procesos requeridos para la
creación y subida de los _ebooks_. Otra consecuencia sería la evasión
de los nuevos intermediarios que funcionan como único distribuidor y
que su trabajo consiste en mandar el libro a diversas plataformas.

¿Si ya no aceptamos los términos abusivos de los distribuidores y en
su lugar hacemos presión para que estandaricen y transparenten sus
prácticas?
