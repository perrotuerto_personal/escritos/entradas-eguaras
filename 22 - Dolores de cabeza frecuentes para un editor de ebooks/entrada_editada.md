# 22. Dolores de cabeza frecuentes para un editor de _ebooks_

Al momento de desarrollar un libro electrónico hay varios elementos que
pueden fallar. Lo primero que sale a flote es la confianza sobre la
capacidad de quien hace el libro. Sin embargo, en varias ocasiones el
_ebook_ no se muestra como debería y esto se debe a cuestiones ajenas 
a la calidad técnica del editor.

Quien encarga la creación o conversión de un libro digital debe conocer
las siguientes particularidades que presenta la edición digital.

En esta entrada se abordan cuestiones básicas que atañen a la producción
los _ebooks_:

* La inexistencia de «página» en los libros digitales.
* La limitación de los diferentes dispositivos en cuestiones tipográficas.
* Las capacidades del formato +++EPUB+++.
* Qué hace un renderizador y cómo afecta a un _ebook_.
* Los distribuidores y su papel.
* Los formatos propietarios, el +++DRM+++ y la minería de datos.

## 1. Una analogía (errónea): la página

En la edición de libros impresos la página es el paradigma para la
lectura y disfrute de una obra. Tras bambalinas, la página también es 
el modelo para la corrección de errores.

¿Cuántas veces hemos leído «te mando los siguientes cambios: en _la
página 5…_»? La página es muy útil para moverse a través de la obra.
Más cuando la obra no se lee, sino que se emplea como referencia.

Un tipo útil de referencia es la que indica donde hay erratas, errores
de formación y más. Esto bajo el supuesto de que la página tiene una
dimensión constante en todos los dispositivos de lectura.

Para desgracia del control editorial, en las publicaciones electrónicas
la página nunca tiene el mismo tamaño. La página, como un lienzo o un
contenedor, no es una analogía lo suficientemente fuerte para
representar el rol que desempeña en la edición.

**La página implica una idea de un orden perenne**. Mientras que el
libro sea libro, la página siempre permanecerá igual, se raye, se moje 
o se rompa.

En un libro electrónico este carácter fijo ya no existe. La «página»
digital es solo una remembranza del papel que desempeñaba durante la
lectura. La tradición editorial trasladó la publicación como pergamino 
a una conformada por varias páginas.

En la edición digital la «página» es solo una convención que da al
lector el mismo tipo de dirección al que está acostumbrado: 
izquierda / derecha → arriba / abajo → anverso / reverso.

Lo más grave que este equívoco no es la dificultad añadida para poder
localizar las referencias a errores. ¿Qué hay de las viudas y huérfanas?
¿De una imagen y su pie de foto anclado? ¿De los callejones y los
desbordamientos?

### La «página» del libro digital no ofrece soluciones tipográficas

En la publicación digital, al ser variable el tamaño de la «página», 
no se puede dar solución definitiva a estos problemas en la formación. 
**Lo que funcione para un tamaño de pantalla o dispositivo no 
necesariamente servirá para otros tipos de dispositivos**. Incluso 
puede ir en detrimento de la experiencia de lectura, como son 
indeseables saltos de línea o desbordamientos.

La cuestión reside en que los recursos necesarios para solventarlos no
los justifican: **el lector promedio es ciego a estos errores de formación**. 
Esto, asimismo, quiere decir que estamos ante dos tensiones en el 
constante devenir del desarrollo de _ereaders_.

Por un lado, lo que el programador y el editor consideran relevantes. 
En no pocas ocasiones quien programa tiene otras prioridades: concibe 
al libro de modo distinto a quien lo edita.

Por el otro, no puede dejarse de lado que el principal desarrollo de 
los _ereaders_ se da en el mundo anglosajón. Las prioridades en el 
[cuidado editorial](https://marianaeguaras.com/el-cuidado-editorial-durante-la-produccion-de-un-libro/)
en inglés difieren con las de nuestra lengua. Ejemplo: la levedad con 
la que se permiten las viudas y las huérfanas.

Cuando nos adentramos al **cuidado editorial de publicaciones
electrónicas en español** no queda sino aceptarlo. La página y todo su
cuidado en la formación en nuestra lengua queda muy mermada cuando
hablamos de «páginas» en los _ereaders_.

¿Qué pasaría si en lugar de rendirnos decidimos, conscientemente,
comenzar el desarrollo de estándares en _ereaders_ amigables a la
tradición editorial en español?

## 2. El culpable directo: el editor-diseñador-desarrollador

Una vez aceptada a regañadientes la limitación que nos da la «página»
del _ebook_, acontecen otros problemas. En principio, estos son
achacables a la persona que desarrolla la publicación.

La falta de profesionalización en la creación de publicaciones
electrónicas provoca que muchas veces la calidad posible no sea
alcanzada. Entre el cuidado del texto como contenido, luego como 
diseño y hojas +++CSS+++, hasta como estructura y etiquetas +++HTML+++ 
acontece una constante transposición de dimensiones del mismo texto.

No es sencillo desarrollar el ojo para **tener capacidad de percibir 
al texto en estas tres dimensiones al unísono**. Lo que parece una 
tarea sencilla, se convierte en un infierno para quien tiene un primer
acercamiento a la publicación digital.

Como lectores o personas ajenas a los procesos requeridos para hacer 
un _ebook_ es sencillo juzgar un libro por lo que se ve. Como es común, 
un libro electrónico tiende a tener una formación más sencilla, así 
como es estéticamente menos placentero. De ahí se concluye que su 
creación ha de ser sencilla.

Nada más descontextualizado que esta suposición. Si el _ebook_ tiende 
a ser «feo» o «simplón» no es porque el formato no permita más, sino
debido a que su dominio exige un largo camino para quien aprendió a
editar a través del sendero visual del diseño editorial.

El formato estándar del libro electrónico permite muchas posibilidades
interesantes. Sin embargo, el desarrollador de _ebooks_ no solo tiene
que aprender esto, sino también tiene que repensar el diseño a través 
de hojas de estilo y mantener la calidad editorial en un contexto de
«páginas fluidas».

A esto se suma que el trabajo hecho en una publicación electrónica, por
lo general, es menospreciado por quienes se dedican a la publicación de
impresos. Como resultado tenemos que muchas veces, aunque ya se cuente
con la capacidad de mejorar el _ebook_, no se lleva a cabo debido a que
el tiempo requerido y el poco reconocimiento no lo justifica.

¿Qué tal si ya, por fin, nos libramos del menosprecio hacia el _ebook_
y, en su lugar, vemos el carácter tripartito de su gestación y la
necesidad de hacerle frente?

## 3. El diligente (y menospreciado): el +++EPUB+++

Entre los formatos digitales, el +++EPUB+++ es un estándar en la
industria con mucha potencia. Su aspecto más conocido es la posibilidad
de **embeber objetos multimedia**.

Otra característica relevante es la **capacidad de poder incluir
JavaScript**. Con este lenguaje de programación se pueden añadir
diversas funcionalidades a un libro. Por ejemplo: animaciones,
formularios, modificación de estructuras y hasta videojuegos.

A esto sumamos el **soporte para personas con deficiencia visual**, a 
la creación de fórmulas matemáticas e incluso la experimentación para 
la adición amena de partituras.

Con las hojas aurales (hojas de estilo auditivas) es posible escuchar 
la obra de manera más natural. Las fórmulas matemáticas ya no tienen 
que ser imágenes, sino texto. Del mismo modo, las partituras pueden
seleccionarse nota a nota y hasta escucharlas con reproductores
+++MIDI+++.

Y no lo es todo, la estandarización del +++EPUB+++ da pauta para que
**todas estas posibilidades puedan verse igual y del mismo modo en
cualquier _ereader_**. Y aquí es donde nos aproximamos a terreno
escabroso…

¿Qué pasa si antes de seguir nos detenemos un poco y nos preocupamos 
por tratar de contemplar todo lo que el +++EPUB+++ tiene por ofrecer, 
en lugar de desaprobarlo y aclamar necesidad de nuevos formatos
(propietarios)?

## 4. Primeros cómplices: los renderizadores

El +++EPUB+++ tiene mucho por ofrecer, pero sus posibilidades por lo
general se han mermado por los renderizadores. Todo dispositivo o
programa que se utilice para leer un _ebook_ es un renderizador.

De las instrucciones y funciones incluidas en un +++EPUB+++, el
_ereader_ o el _software_ procede a pasar de ese código a un resultado
legible. Esto es el proceso de renderización: la **generación de una
imagen a partir de un conjunto de comandos**.

Por desgracia, aunque existen estándares sobre cómo debería renderizarse
el _ebook_, por limitaciones de _hardware_ o decisión del desarrollador,
esto queda como sugerencia.

Su efecto ya es muy conocido para muchos de nosotros: **el libro nunca
se ve de la misma manera**. Por ejemplo, hay ausencia de versalitas,
falta de corte silábico o diferencia en el interlineado o en los
márgenes.

Un dispositivo antiguo no podrá dar el mismo desempeño cuando muestra
las características más complejas e incluso, simplemente, no las
ejecutará.

También existen otros casos en que desarrolladores como Apple o Amazon
restringen algunas posibilidades. Ejemplos: Amazon no permite la adición
de ninguna funcionalidad de JavaScript y el soporte de +++CSS+++ tiene
sus limitantes; Apple bloquea el uso de formularios que [ni con JavaScript pueden eludirse](http://epubsecrets.com/what-ibooks-does-behind-the-scenes.php).

La aplicación de Google, entre las más populares, es la menos limitada.
No obstante, de vez en cuando aparece un [_glitch_](https://es.wikipedia.org/wiki/Glitch) 
en el que se crean saltos de línea forzados. El párrafo a la vista se 
ve mal formado aunque su estructura esté limpia.

Esta disparidad en el soporte en no pocas situaciones **provoca una
tensión entre quien desarrolla el _ebook_ y su cliente.** En más de 
una ocasión, tanto el cliente como yo, hemos tenido que ser lo 
suficiente pacientes para demostrar **que la ausencia de una 
característica o la aparición de un error en la formación no es por la 
mala calidad del código sino una cuestión del renderizador**.

Por estos motivos, el renderizador menos problemático que he encontrado
es [Calibre](https://calibre-ebook.com/). En particular, es el que siempre 
recomiendo porque además es _software_ libre. La interfaz puede parecer 
burda, pero su respeto a los estándares es muy elevado.

¿Que tal si, de una buena vez, caemos en cuenta que las posibilidades
del _ebook_ están constreñidas a las características de cada uno de 
los dispositivos?

## 5. Segundos cómplices: los distribuidores

El _ebook_ puede estar ya desarrollado para sacarle su mejor partida 
en una amplia gama de dispositivos. Sin embargo, aun así nos topamos 
con **dos grandes inconvenientes de los distribuidores**.

### A. La conversión a formatos propietarios

El primero es que algunos de ellos ---guiño de nuevo a Amazon y Apple---
no ponen a la venta el formato estándar que hemos trabajado. En su
lugar, **los convierten en sus formatos propietarios**.

La argumentación es que sus propios formatos están optimizados para sus
renderizadores. Por ello, lo aconsejable es utilizar sus herramientas 
de creación de _ebook_ para producir sus formatos de manera directa.

Además de que sus formatos propietarios son herederos directos de
estándares abiertos, estos incluyen características adicionales que
habilitan el +++DRM+++ y la minería de datos.

A esto se suma el aumento de horas de trabajo para poder crear una
publicación en distintos formatos si se opta por la vía de la
bifurcación del proyecto en lugar de la conversión.

#### La inutilidad del +++DRM+++

El candado digital que evita la reproducción no autorizada de una obra
(el +++DRM+++) es, en mi opinión, una de las cuestiones en las que la
edición digital ha despilfarrado una gran cantidad de recursos. **Este
candado nunca se ha mostrado lo suficientemente seguro** para las
personas que saben cómo eliminarlo.

El aprendizaje sobre cómo hacerlo no es complejo. Con un uso sencillo 
de la terminal y un par de _plugins_ de Calibre es posible «liberar» 
las obras.

En lo personal, es un procedimiento común cuando adquiero algún libro 
en Amazon o en sitios que cuentan con +++DRM+++ de Adobe. No por la
supuesta «piratería» que el +++DRM+++ pretende evitar, sino porque me
desagrada la idea de que su uso es limitado a pesar de haber comprado 
el _ebook_. Este desagrado muta en necesidad cuando estas tecnologías
propietarias no dan soporte a sistemas operativos abiertos.

#### Minería de datos o la recolección de información

La minería de datos es una cuestión muy delicada y que casi no se ha
comentado en discusiones relativas a la edición y la publicación
digitales.

Bajo el eufemismo «análisis de hábitos de lectura» lo que varios
renderizadores llevan a cabo es una recolección de información que
posteriormente será procesada por sus compañías desarrolladoras.

Con todas las relaciones encontradas es posible **monetizar la manera 
en cómo leemos libros**. ¿Maravillado porque Google, Amazon o Apple 
siempre te sugiere buenos libros? ¿Asombrado de las coincidencias entre 
lo que lees y la publicidad que te muestra? No es fortuito, **ellos 
buscan el medio para venderte algo, incluso a través de lo que lees**.

### B. La aprobación de la publicación de _ebooks_

El segundo inconveniente no está en el lado del usuario hecho lector,
sino en el del editor. Para quienes suben libros a distintas
plataformas, en más de una ocasión, es un martirio que la publicación
sea aprobada.

Dos ejemplos. Uno, Apple solo permite la subida de libros a través de
iTunes Producer que, como pueden adivinar, solo está disponible para
macOS. Y Amazon ofrece mucho menos regalías cuando el _ebook_ no es
exclusivo de su plataforma.

Las soluciones me parecen limitadas. Una consiste en pedir prestada una
Mac para subir el libro. La otra es que, como Amazon es dueño de su
plataforma, uno elige si acepta o no sus condiciones.

Como usuario y como editor **no deberíamos estar limitados al uso de
_software_ específico para el desarrollo y comercialización de nuestros
libros**. Tampoco tendríamos que ver mermadas nuestras posibilidades de
venta o de regalías según las condiciones de cada distribuidor.

A esto se suma la **ausencia de estándares en la distribución de los
_ebooks_**. No es fortuito ni inevitable que se tengan que hacer
procedimientos semejantes una y otra vez según la plataforma.

En realidad, todas las plataformas necesitan la misma información para
poder colocar a la venta el libro. Si estas optaran directamente por 
una publicación estándar, como el +++EPUB+++, también se haría posible
estandarizar su distribución.

#### Una idea para automatizar la distribución de _ebooks_

La idea es sencilla. Cada distribuidor requiere acceso al _ebook_, a 
la información para su venta y hasta algunas impresiones de pantalla. 
La clase de archivo que utiliza iTunes Producer para procesar toda 
esta información es una carpeta. Esta contiene, entre las capturas de
pantalla y el _ebook_, un +++XML+++ con toda la información necesaria
para la venta.

Podría encontrarse un esquema +++XML+++ estandarizado para esta
información. Y, a partir de ahí, juntarlo con el libro y sus capturas 
de pantalla en una carpeta comprimida como +++ZIP+++. Con los accesos
necesarios incluso se podría automatizar su subida mediante +++FTP+++ o
[scp](https://www.garron.me/es/articulos/scp.html).

Con esto se abriría la posibilidad de poder **automatizar la
distribución** y así tener pleno control de todos los procesos
requeridos para la creación y subida de los _ebooks_.

Otra consecuencia sería la evasión de los nuevos intermediarios que
funcionan como único distribuidor; cuyo trabajo consiste en mandar el
libro a diversas plataformas.

¿Qué pasaría si no aceptamos los términos de los distribuidores y, en 
su lugar, hacemos presión para que se estandaricen y transparenten sus
prácticas?

<aside class="espacio-arriba2">

###### Sobre esta entrada

* Título original: «Dolores de cabeza frecuentes para un editor de _ebooks_».
* Título en el _blog_ de Mariana: «Dolores de cabeza frecuentes para un editor de _ebooks_».
* Fecha de publicación: 28 de agosto del 2018.
* [Redacción original](https://gitlab.com/NikaZhenya/entradas-eguaras/blob/master/22%20-%20Dolores%20de%20cabeza%20frecuentes%20para%20un%20editor%20de%20ebooks/entrada.md).
* [Revisión de Mariana](https://marianaeguaras.com/dolores-de-cabeza-frecuentes-para-un-editor-de-ebooks/).

</aside>
