# Tras bambalinas de «Historia de la edición digital»

Como se había señalado en el [repositorio](https://github.com/NikaZhenya/historia-de-la-edicion-digital)
del artículo «[Historia de la edición digital](http://marianaeguaras.com/historia-de-la-edicion-digital/)», 
en esta ocasión se mencionan los pasos llevados a cabo para su edición 
y publicación.

## Proceso de desarrollo

>A continuación se comentarán unas impresiones de pantalla omitiendo la 
parte técnica, ya que los *scripts* utilizados están pasando por grandes 
cambios en aras de la posibilidad de una interfaz gráfica.
[Estas son las herramientas](http://herramientas.perrotriste.io/), por si
quieres echarle un ojo.

### 1. Clonación del repositorio, 14:22 hrs.

![Clonación del repositorio, 14:22 hrs.](recursos/img1.png)

El proyecto es un repositorio para 1) tener un gestor de versiones que
nos permite puntos de guardado, evitándose el respaldo de archivos cada
vez que se termine o se empiece una tarea importante, y para 2) habilitar
la conexión a un servidor para salvaguardar el proyecto si la computadora
deja de funcionar.

Para este caso utilizamos [GitHub](http://github.com/) como el servidor
de nuestro repositorio, ya que es una plataforma estable y pública, por 
lo que no tenemos que preocuparnos por ese mantenimiento y, lo más importante, 
**¡para que cualquiera pueda descargarlo!**

Por ello se genera el repositorio en GitHub y a continuación se clona
(léase «descarga») a la computadora.

Por supuesto es posible crear repositorios privados para que solo nosotros
o un equipo de trabajo tenga acceso, pero no es lo que se pretende en esta
publicación.

### 2. Adición de recursos, 14:24 hrs.

![Adición de recursos, 14:24 hrs.](recursos/img2.png)

A continuación se añaden al proyecto las imágenes y el texto editado por 
Mariana. Y antes de seguir con el siguiente paso, preferí ir a comer 
porque lo había olvidado, je.

### 3. Revisión del archivo original, 15:05 hrs.

![Revisión del archivo original, 15:05 hrs.](recursos/img3.png)

Con Mariana estuvimos editando el texto entre el formato RTF y ODT, ya 
que no cuento con Microsoft Office ni tenía el deseo de trabajar con los 
formatos DOC o DOCX.

Entre el RTF y ODT luego suceden cosas «extrañas» en el formato, por lo 
que se ha de examinar el archivo para constatar que todo está en orden.

### 4. Conversión del archivo original, 15:08 hrs.

![Conversión del archivo original, 15:08 hrs.](recursos/img4.png)

Mediante [Pandoc](http://pandoc.org/) se convierte el texto original a 
Markdown. Este proceso es uno de los más breves e importantes 
ya que así obtenemos el formato más apto para nuestro archivo madre.

### 5. Revisión del archivo madre, 15:12 hrs.

![Revisión del archivo madre, 15:12 hrs.](recursos/img5.png)

Markdown es un lenguaje de marcado ligero, permitiéndonos darle estructura 
al texto (encabezados, bloques de cita o itálicas, p. ej.) con
una sintaxis fácil de aprender y de leer.

La consecuencia de esta versatilidad es, sin duda, una simplificación
en la estructura. Markdown es un lenguaje con el que podemos determinar
una [gran variedad de elementos](https://daringfireball.net/projects/markdown/syntax.php)
pero sus posibilidades se quedan cortas si lo comparamos con HTML o TeX.

Sin embargo, **es en su simplicidad por el que Markdown es
el lenguaje más óptimo para el archivo «madre»** de cada uno de los formatos para
nuestra publicación. La idea detrás de esto es ir de lo simple a lo complejo,
del Markdown a otros formatos que, por su naturaleza, requieren ajustes
particulares.

Así que en este pasó solo se da un vistazo al archivo madre, más adelante
es donde agregaremos otros elementos.

### 6. Creación del proyecto para EPUB, 15:12 hrs.

![Creación del proyecto para EPUB, 15:12 hrs.](recursos/img6.png)

Una de las primeras herramientas de Perro Triste es `pt-creator`, la cual
nos crea un proyecto base para un EPUB. Con esto evitamos empezar el proyecto
desde cero o a partir de la copia de otro anterior, ya que nos implementa
una estructura de archivos y carpetas convencional a cualquier EPUB.

### 7. Conversión del archivo madre a HTML, 15:14 hrs.

![Conversión del archivo madre a HTML, 15:14 hrs.](recursos/img7.png)

Para el EPUB requerimos al menos un archivo HTML, el cual tiene su origen
en el archivo madre que convertimos en HTML a través de Pandoc.

Aquí es cuando también se aprovecha para añadir elementos adicionales al 
HTML, como son las clases de párrafos, los identificadores o
las imágenes.

### 8. División del archivo HTML, 15:32 hrs.

![División del archivo HTML, 15:32 hrs.](recursos/img8.png)

Por lo general un EPUB tiene un documento XHTML por cada sección de la 
obra, así que ahora dividimos el archivo HTML por medio de `pt-divider`.

Este *script* divide el documento de entrada cada vez que encuentra un
encabezado de primera jerarquía (`h1`), permiténdonos especificar el tipo
de sección que se trata y generando automáticamente un XHTML.

¡Ups, alguien llama a la puerta! Hora de suspender un poco...

### 9. Creación del EPUB, 16:08 hrs.

![Creación del EPUB, 16:08 hrs.](recursos/img9.png)

Ahora ya contamos con todo lo necesario para nuestro EPUB, por lo que procedemos
a crearlo con `pt-divider`. Este es uno de los *scripts* más importantes 
ya que **evita que nos preocupemos de la correcta
introducción de metadatos o de la creación de todos aquellos archivos propios
del EPUB**, como las tablas de contenidos. Así pues, poco a poco nos acercamos
al ideal donde quien edita o quien diseña puede exclusivamente centrarse
en su trabajo.

### 10. Validación del EPUB, 16:12 hrs.

![Validación del EPUB, 16:12 hrs.](recursos/img10.png)

Crear el EPUB es una cuestión, pero otra es desarrollar correctamente un
EPUB. Para saber si nuestro *ebook* tiene coherencia interna, utilizamos
[EpubCheck](https://github.com/IDPF/epubcheck/releases), el validador oficial.

Como `pt-divider` automatiza la creación de la «médula» del EPUB, los posibles
errores o advertencias que nos indique EpubCheck estarán relacionados a nuestra
labor de marcado o de inclusión de recursos. (En ocasiones bien específicas
algún error es ocasionado por una herramienta de Perro Triste que se van
depurando conforme se van identificando).

Si el EPUB tiene errores, solo basta con corregirlos y regenerar de nuevo
el EPUB con `pt-recreator`, un proceso tan sencillo que incluso este *script*
guarda los metadatos ya introducidos.

Para evitar la gran mayoría de los errores, recomiendo dos cosas: 1)
**usar formatos XHTML en lugar de HTML**, ya que así no se permiten ambigüedades
en las etiquetas, y 2) **ver siempre los archivos en Firefox** antes de crear
el EPUB, debido a que este explorador **libre** no te muestra el archivo 
si tiene errores, caso contrario a Safari o Chrome.

### 11. ¡EPUB listo!, 16:14 hrs.

![¡EPUB listo!, 16:14 hrs.](recursos/img11.png)

**En menos de dos horas terminamos el EPUB, sin errores y con un gran control
sobre la estructura y el diseño** (y eso que una hora se nos fue entre
la comida y la llamada a la puerta), ¿acaso el proceso podría simplificarse y estar más
enfocado a las necesidades editoriales? ¡Sí, es en lo que estamos trabajando!

### 12. ¡Convesión del EPUB a MOBI!, 16:14 hrs.

![¡Convesión del EPUB a MOBI!, 16:14 hrs.](recursos/img12.png)

Para el archivo legible para Kindle no tenemos que hacer gran cosa.
Amazon tiene [KindleGen](https://www.amazon.com/gp/feature.html?ie=UTF8&docId=1000765211)
a nuestra disposición, una herramienta que nos permite convertir el EPUB
a su formato privativo.

**¡En menos de un minuto ya tenemos listo el *ebook* para Kindle!**

### 13. Creación del proyecto para LaTeX, 16:16 hrs.

![Creación del proyecto para LaTeX, 16:16 hrs.](recursos/img13.png)

Ahora es momento de concentrarnos en la creación del PDF, para esto no usaremos
el *software de facto* en el mundo editorial (<s>InDesign</s>), sino una
herramienta más antigua y potente: TeX.

Para facilitarnos la creación del EPUB, emplearemos un conjunto de [macros](https://es.wikipedia.org/wiki/Macro)
conocido como LaTeX. Por el momento no existe un *script* de Perro Triste 
para crear un proyecto base de LaTeX, por lo que se copia y pega manualmente 
una plantilla que se encuentra en el repositorio de estas herramientas.

### 14. Conversión del archivo madre a TeX, 16:18 hrs.

![Conversión del archivo madre a TeX, 16:18 hrs.](recursos/img14.png)

De nueva cuenta convertimos el archivo madre a través de Pandoc, pero ahora
para tener un formato TeX. A partir de aquí se empieza la labor
de cuidado editorial característico de un impreso, por lo que nos tomamos un 
tiempo para corregir cajas, viudas, huérfanas, etcétera.

### 15. ¡PDF listo!, 17:57 hrs.

![¡PDF listo!, 17:57 hrs.](recursos/img15.png)

Después del meticuloso cuidado en LaTeX (ni tanto, porque
Mariana encontró varios detalles al PDF), **¡obtuvimos un PDF en poco
más de hora y media!**

El PDF puede modificarse para que sea una salida apta para impresión,
pero en este caso solo optamos por la posibilidad de una lectura digital.

### 16. Creación del proyecto para GitBook, 18:09 hrs.

![Creación del proyecto para GitBook, 18:09 hrs.](recursos/img16.png)

Por último, también quisimos tener una opción completamente *online*. Para esto optamos por
[GitBook](https://www.gitbook.com/), un proyecto creado por GitHub inicialmente
para la creación de documentación de *software*.

### 17. Clonación del repositorio de GitBook, 18:18 hrs.

![Clonación del repositorio de GitBook, 18:18 hrs.](recursos/img17.png)

Una vez que configuramos el libro en GitBook, procedemos a descargar el
repositorio. En este punto es donde la publicación comprende dos repositorios, 
[uno para el formato de GitBook](https://github.com/NikaZhenya/historia-de-la-edicion-digital-gitbook) 
y otro [para el resto de los formatos](https://github.com/NikaZhenya/historia-de-la-edicion-digital). 
Esto se debe a que GitBook automáticamente actualiza la publicación cuando 
se mandan cambios al servidor.

Cabe la posiblidad de tener solo un repositorio, pero por descuido y pereza
no se unificaron...

### 18. División del archivo madre, 18:32 hrs.

![División del archivo madre, 18:32 hrs.](recursos/img18.png)

El formato que utliza GitBook es Markdown, por lo que solo es necesario
crear una serie de archivos en este formato para copiar y pegar cada una
de las secciones. Posteriormente en otro Markdown se crea la tabla de contenidos
y ya tenemos todo para publicar este formato.

### 19. ¡GitBook listo!, 19:26 hrs.

![¡GitBook listo!, 19:26 hrs.](recursos/img19.png)

**Después de poco más de una hora, el artículo ya está disponible en línea**
cuando se mandan los cambios al servidor.

**En un lapso de cinco horas desarrollamos cuatro formatos del
*ebook***, que cualquier persona puede descargar o leer en EPUB, PDF, MOBI
o en línea. Mientras tanto, ¿ya hace hambre de nuevo, no?

## *Single source* y *online publishing*

A partir de un archivo madre se obtuvieron tres formatos de una misma publicación 
([EPUB](https://github.com/NikaZhenya/historia-de-la-edicion-digital/blob/master/ebooks/produccion/historia-de-la-edicion-digital.epub?raw=true),
[PDF](https://github.com/NikaZhenya/historia-de-la-edicion-digital/raw/master/ebooks/produccion/historia-de-la-edicion-digital.pdf) y
[GitBook](https://nikazhenya.gitbooks.io/historia-de-la-edicion-digital/content/) ), 
así como se creó el formato [MOBI](https://github.com/NikaZhenya/historia-de-la-edicion-digital/blob/master/ebooks/produccion/historia-de-la-edicion-digital.mobi?raw=true)
a partir del EPUB. Esto es un ejemplo del *single source publishing*, en 
donde a partir de un tronco en común (en este caso el Markdown), se crearon
distintas «ramas» para cada uno de los formatos.

De esta manera todos tienen un elemento en común pero al mismo tiempo contemplan
sus particularidades, haciendo que estos cambios específicos no afecten 
al resto de los formatos.

En el caso de existir algún problema, el *online publishing* nos permite
recuperar información a partir de puntos de control e incluso tener el texto
disponible en línea con una simple actualización. Por ejemplo, a partir de 
ahora desde los distintos enlaces puede descargarse o leerse el artículo 
con la seguridad de que siempre se accederá a la versión más actualizada,
¡porque no tenemos que copiar y pegar archivos en otro lugar!

Por las imágenes mostradas, más de uno pensará que se trata de muchos procesos 
complejos para la mayoría de las personas que publican un libro... Lo único
que puedo mencionar es que ese es el motivo por el que en Perro Triste estamos
desarrollando estas herramientas, así como en Nieve de Chamoy es donde se
prueban y afinan en el día a día del quehacer editorial.

Al final, ¿qué otras posibilidades existen para facilitar y automatizar
la creación de diversos formatos con tiempos semejantes a un día de jornada
laboral? Existe la convicción de que quien edita o quien diseña no debe 
de dejar de lado el cuidado que exigem sus profesiones. Pero para ello debe
de haber un empoderamiento tecnológico que convierta las actuales exigencias
del mundo editorial en posibilidades de mejora, en lugar de dificultades.

## Turno de Mariana

Una vez terminado el desarrollo de los diversos formatos, Mariana cotejó
cada uno de los formatos para depurar errores. Este proceso de repechaje
también exige una atención especial (está en la lista de deseos de Perro Triste),
pero por el momento nos acotamos a modificar y recrear cada uno de los archivos.
Proceso que no consume más tiempo que el primer desarrollo, por supuesto...


