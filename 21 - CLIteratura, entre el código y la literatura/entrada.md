# CLIteratura, entre el código y la literatura

¡[No todo es publicación estándar](https://marianaeguaras.com/appbooks-y-realidad-aumentada-no-todo-son-ebooks/)!
Lo más llamativo de las tecnologías digitales en relación con el mundo
del libro es, sin duda, [la gamificación de la edición](https://marianaeguaras.com/el-videojuego-y-el-libro-la-gamificacion-de-la-edicion/).
Desde los _appbooks_ hasta la realidad aumentada o virtual, en diversos
eventos relativos al libro se anuncian estos productos como el «futuro»
del libro.

Aunque más bien es el «presente» de las posibilidades de la edición
digital, muchos de estos productos tienen una relación muy definida
con las capacidades técnicas digitales que vale la pena resaltar.

## El código como infraestructura

Lo más llamativo de este tipo de publicaciones no estandarizadas —no
utilizan los mismos procesos, las técnicas varían y hay casi tantas 
metodologías como productos posibles— es el dinamismo y la «pérdida»
de estructura. **Lo que se pierde no son conocimientos tradicionales de
cuidado editorial, sino una noción que era ley en el reino exclusivo
del impreso: la página**.

La página limitaba posibilidades a la publicación, pero también era
un lienzo que daba fuentes ilimitadas de creatividad literaria,
tipográfica y gráfica. La publicación estandarizada hace una metáfora
de esta noción, pero en los procesos no estandarizados de edición
digital la página en muchas ocasiones es sustituida por la idea de
escena.

En una escenificación tenemos dinamismo: el diálogo y la escenografía
se orientan a una narrativa que no espera a ser leída. La historia
avanza y el lector se hace espectador y a veces actor.

En el caso de las escenas planificadas para la producción de 
_appbooks_, ya se busca que el espectador sea constantemente un agente
dentro de la misma narrativa. Sea desde una simple navegación interactiva
hasta el completo sumergimiento en la historia, el espectador se hace
usuario.

Y en este traslado del lector al usuario del libro lo que resalta son
el tipo de recursos a los que se tiene acceso y cómo estos son presentados
y modificados por el usuario. Sonidos, videos, animaciones, física
simulada, análisis de datos, uso de gestos o creación de mundos 
tridimensionales: la edición engloba ya más elementos, ¿editores, 
pueden con esta tarea?

Pero esto no sería posible sin una base informática que permita el
uso de dichos recursos. **El código viene a ser el pilar para la
edición digital** y no hay caso más evidente de esta necesidades que
las publicaciones digitales no estandarizadas.

Sin el código no hay manera de publicar estos y casi todos los productos 
editoriales. O si somos un poco severos: **la edición ha quedado 
subordinada al código**. ¿No lo crees así? ¿Usas una computadora para
editar o publicar tus libros? Bienvenido, el código te respalda.

Más allá de las limitaciones del programa o de su uso, rara vez se 
vuelve un tema de conversación la dependencia tecnológica de la edición.
De manera inexplicable o mera renuencia, el código se percibe como dado.

A nosotros, editores, ya se nos ha otorgado el código en su forma más 
amigable: como un programa con interfaz gráfica, el código compilado 
en archivos ejecutables. Por el código la edición se ha transformado, 
para bien o para mal.

## El código como propiedad

Cuando se habla de los programas para el quehacer editorial, la primera
pregunta que surge no es «¿cómo lo uso?», sino «¿cómo lo adquiero?».
Las respuestas más comunes involucran su compra o su pirateo. **Tan 
normal se ha vuelto esta dependencia que se da por sentado que se ha
de pagar o violar derechos de autor para poder editar**.

Al final el código, aunque ya yace ahí en una paquetería de Adobe o
de Microsoft, lo hace alguien, le pertenece y tiene la intención de
vivir de ello. Por lo tanto una compensación económica es razonable
—o un «discúlpeme, si tuviera dinero le pagaría»—.

La cuestión no es que los programadores tendrían que dar gratis su
trabajo, sino la preocupación de quiénes son los que al final le
sacan un mayor provecho y para qué. En el caso de la edición, este
desasosiego son las cuestiones de cuál nuevo programa vendrá a sustituir
al anterior, qué tendremos que aprender para no quedar en desventaja,
qué habremos de olvidar porque será conocimiento inaplicable; pero,
principalmente, ¿cuánto tendremos que desembolsar para pagar por
_software_, _hardware_ o capacitaciones? ¿Qué de nuevo hay en las
ferias del libro que son la panacea _reloaded_ de las penurias 
editoriales?

**Cuando no se tiene idea de cómo funcionan las bases de tu profesión,
es fácil confundir un programa con un método de trabajo y la dependencia
tecnológica con el «futuro» de tu campo**. La falta de estandarización
no es solo por la efervescencia propia de la vanguardia, sino también
por interés económico en torno a cómo se realizará el traslado de
dependencia tecnológica del grueso editorial. Si se trata del futuro,
es el futuro de la presente economía dependiente de la edición al código.

Aunque viendo para atrás nos parezca que la imprenta fue un éxito que
cambió al sector editorial, muchas veces se nos olvida que la rotación
fue relativamente lenta y que existieron muchas técnicas y tecnologías
compitiendo para _abrir_ el modo de publicación de los amanuenses.

En esta «batalla» —no tanto porque así haya sido sino por cómo [varios
amanuenses se sintieron amenazados](http://gen.lib.rus.ec/book/index.php?md5=60B7C01C3FE0D9FCDA4629736BB3C382)—
también nos falta recordar que la expansión de las tecnologías de la
impresión fueron en parte gracias al plagio: todas esas personas que
tuvieron acceso al funcionamiento de la maquinaria para replicarla.

## El código como bien común

Sin embargo, veo el código de los archivos ejecutables y solo veo
unos y ceros, ¿cómo funciona todo eso?, ¿cómo puedo replicarlo?, pero,
con mayor interés, ¿cómo hago mi trabajo si desconozco el funcionamiento 
de la maquinaria que me crea mis libros?

O no nos vayamos tan lejos, veo el texto de una obra ya publicada,
la copio para editarla y solo veo contenido, ¿cuál es su estructura?,
¿cómo fue diseñado? Me quedo con pura información que intento dar 
formato con mis escasos recursos informáticos para producir una publicación
cuya calidad de antemano sé que será inferior al original. Puedo ser 
usuario y lector de obras privadas o abiertas, pero mis deseos de ser 
editor quedan limitados.

**Algo tiene que estar mal si para poder aprender a editar tengo que
pagar por programas o archivos que no conozco cómo funcionan** y donde
mis posibilidades ya están constreñidas por lo que estos ficheros me 
ofrecen.

De esta manera surge la idea de que el código no es propiedad privada,
sino una cuestión de interés público. El campo de la edición no es el
único que ha dado, sin mirar las consecuencias, su soberanía al código
propietario. «[El código es ley](http://gen.lib.rus.ec/book/index.php?md5=73413C7A46D72E082C1C4DDE60E0A179)»
y si no aprendemos cómo funciona, habrá otros que saquen ventaja de ello.

¿Paranoia? ¿Quiénes fueron los mayores beneficiarios en los traslados 
de dependencia tecnológica entre PageMaker, QuarkXPress y Adobe, o 
entre los diversos procesadores de texto hasta Microsoft Office? Mientras
tanto, TeX, con ya casi treinta de años, sigue sólido, sin cambios
drásticos y con una comunidad pujante que crea libros con alta calidad
tipográfica.

En el caso de la edición no estandarizada también tenemos una fuerte
competencia entre entornos de desarrollo propietarios y los de código
abierto. Las opciones propietarias son más publicitadas y, en varios
casos, más sencillas de usar. Sin embargo, con el _software_ libre o
de código abierto al menos estoy patente que, aunque no sea programador,
tengo que tener nociones de cómo funcionan mis programas, y este tipo
de *software* me lo permite gracias a su documentación y a que puedo
leer el código.

**Pero como editor, ¿por dónde empiezo?** ¡Tanto conocimiento que se 
me presenta tan ininteligible como fórmulas químicas y matemáticas!, 
cuando mi preparación ha sido principalmente humanística o artística…

## La literatura como pedagogía

¿Y si mostramos la infraestructura informática que está tras 
bambalinas? ¿Qué tal si hacemos una publicación que a la par que se
lee, se aprende un poco de los procesos ocultos en la edición? ¿Y si
a partir de la literatura aprendemos a usar la computadora de otra
manera? Estas preguntas me llevaron a la creación de [CLIteratura](https://cliteratu.re/).

**[CLIteratura](https://cliteratu.re/) es un pequeño proyecto donde se aprende a utilizar una
[interfaz de línea de comandos](https://es.wikipedia.org/wiki/Interfaz_de_l%C3%ADnea_de_comandos) 
en un entorno emulado en el explorador _web_ y mediante la lectura**. 
«CLI» de _command-line interface_ y «LIteratura» porque el medio
el medio de aprendizaje son los textos literarios.

El proyecto surgió debido a lo que he vivido dando talleres y a lo que
he aprendido sobre el modo de hacer libros. Un cambio que considero
fundamental en mi vida profesional y por la cual Mariana me ha dado
este espacio es haber empezado a ver los libros como estructuras,
como archivos cuyo formato condiciona ya la calidad editorial y gráfica
del producto final.

El giro no fue espontáneo, ha sido un proceso muchas veces tortuoso 
donde la potencia de la [terminal bash](https://es.wikipedia.org/wiki/Bash)
no solo me ha ahorrado mucho tiempo, también me ha permitido aumentar
la calidad editorial y técnica de mis publicaciones, así como fue
lo que me motivó a crear Pecas: ¿por qué iba a dejar todos esos _scripts_
que usaba para mi trabajo y desde bash como una propiedad privada si 
sé que puede ser al menos el primer escalón para que otros se den cuenta 
que **la edición es también un método y el texto es nada sin estructura**?

Pero en los tiempos acotados de talleres o pláticas, tengo que admitir
que, cuando muestro el modo como trabajo, los asistentes quedan más 
confundidos y hasta decepcionados de pensar que ellos no podrán tener
el mismo control en su trabajo.

Como la mayoría de los eventos a los que soy invitado están orientados 
a un público versado en humanidades y artes, he observado que el uso 
de muchos ejemplos concretos y de analogías con su campo les permite 
comprender una serie de nociones que una documentación técnica no 
podría lograr.

Por estos motivos durante un tiempo estuve buscando una vía en la que:

* La infraestructura de la información fuera evidenciada, aunque fuese
  emulada.
* Las humanidades y las artes fungieran como nexo pedagógico, sino que
  mero pretexto.
* La dinámica fundiera el fomento a la lectura con la enseñanza
  informática, pese a que bien podría dar un producto híbrido que ni
  es de aquí ni de allá.

No deseaba que el recurso informático quedara escondido y se diera
como dado, sino que se evidenciara y el usuario se familiarizara 
mientras juega con la herramienta.

Con estas ideas surgió [CLIteratura](https://cliteratu.re/). **Por el
momento solo cuenta con un módulo: «Casa tomada» de Julio Cortázar para
aprender a moverse sobre la terminal y a usar las [rutas relativas
o absolutas](https://es.wikipedia.org/wiki/Ruta_(inform%C3%A1tica))**.

Hasta estas fechas solo cuenta con soporte bilingüe inglés-español.
Si tu sistema operativo no está en español, de manera automática te
cargará la versión en inglés ([la traducción de «Casa tomada» es de
Paul Blackburn](https://jessbarga.wikispaces.com/file/view/Cort%C3%A1zar,+House+Taken+Over.pdf)).

Además, quisiera seguir incluyendo más módulos, pero sería extraordinario
marcar el rumbo de desarrollo mediante su retroalimentación. Úsalo,
critícalo o descártalo, todo eso sirve para saber hacia dónde dirigir
CLIteratura.

## Nota sobre el origen técnico de CLIteratura

La base del emulador de CLIteratura fue desarrollada por
[Clark DuVall](https://github.com/clarkduvall), el cual llamó «[jsterm](https://github.com/clarkduvall/jsterm)».

Lo curioso de jsterm es que fue hecho para su sitio personal, de
manera particular como una manera de mostrar su currículo como ingeniero.

¿Cómo se pasó de un sitio para mostrar un currículo a uno para aprender 
a usar la terminal con obras literarias? Mediante la apertura del código.
**Dar acceso al código no solo permite ahondar en la formación del
mismo campo de estudio, también posibilita usarlo de maneras inesperadas**.
