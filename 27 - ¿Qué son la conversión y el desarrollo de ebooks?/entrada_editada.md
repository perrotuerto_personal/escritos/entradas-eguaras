# 27. Cuál es la diferencia entre la conversión de _ebooks_ y desarrollo de _ebooks_

El mercado ofrece una gran diversidad de proveedores de servicios
para la producción de _ebooks_ estandarizados ---+++EPUB+++,
iBooks, +++MOBI+++---. Unos son más baratos que otros; algunos
mejor que los demás.

Entre los editores y los autores rara vez está clara la diferencia
de tarifas de producción de _ebooks_. Por lo general, esto obedece
a la manera en cómo se produce el libro electrónico. Existen
al menos dos medios para lograr tal cosa: mediante la conversión
de _ebooks_ o su desarrollo.

Veamos en qué consiste cada una de estas vías.

## Conversión: en la piel del _ebook_

**En apariencia, el medio más barato y sencillo para producir
libros electrónicos es, sin duda, la conversión**. A través de
un programa, un _plug-in_ o una plataforma _web_ el usuario convierte
un archivo a diferentes formatos.

Muchas de las veces la conversión de _ebooks_ tiene como archivos
base documentos de Word o publicaciones maquetadas en InDesign.

Si se trata de documentos de Word, el procedimiento más sencillo
es utilizar las herramientas que nos ofrece el gestor de bibliotecas
[Calibre](https://calibre-ebook.com). Con un par de clics tendremos
diversos formatos electrónicos de nuestra obra, incluyendo +++EPUB+++
para cualquier dispositivo (excepto Kindle) y +++MOBI+++ para
cualquier _ereader_ de Amazon.

Cuando se trata de documentos de InDesign, la producción del
+++EPUB+++ se lleva a cabo a partir de una simple exportación
del documento a ese formato. Con ello, es posible subir ese archivo
a cualquier distribuidor y serán ellos quienes se encarguen de
convertirlo a sus propios formatos propietarios.

Muy sencillo, ¿no es cierto? Sin embargo, podemos llegar a tener
al menos tres grandes problemas:

1. Los metadatos de nuestra obra son erróneos.
2. Los distribuidores la rechazan por tener errores técnicos.
3. El archivo está bien, pero su diseño es un desastre.

Es aquí donde **el servicio de conversión de _ebooks_ muta en
curaduría**. Varias de estas dificultades pueden solventarse
usando las configuraciones avanzadas de exportación o usando
utilidades con interfaz gráfica. Es común que varios de estos
detalles se arreglen con un par de clics.

No obstante, para un usuario promedio puede llegar a ser un proceso
tortuoso. Esto hace que se opte por contratar un proveedor cuyo
trabajo será convertir el archivo con la calidad deseada.

### Posibles inconvenientes

En algunas ocasiones, las dificultades pueden ser más complejas.
Uno de los casos que puede darse es una **serie de errores**,
porque la estructura +++HTML+++ no es correcta. Otro ejemplo
más es una **deficiencia en el diseño**, que requiere modificación
de la hoja de estilos +++CSS+++.

Aunque se trata de aspectos y lenguajes distintos, estos y **varios
de los inconvenientes** más graves en los procesos de conversión
**tienen un mismo origen: código redundante, ininteligible y
sobrante**.

Esto se debe a la manera en cómo se convierte el documento a
partir de las estructuras generadas por el editor o el autor.
En efecto, varias de las dificultades se originan porque el documento
no fue marcado a partir de estilos de párrafo o de caracteres
y, en su lugar, se empleó la edición directa de cada una de las
estructuras que comprenden la obra.

En algunos casos sí se usan los estilos. Sin embargo, en aras
de mejorar el aspecto visual del impreso, la persona que diseña
hace uno que otro cambio manual: un ajuste de puntaje, algunos
saltillos de línea, etcétera.

Todo esto que no se ve, _pero que está presente en la estructura_
de una u otra forma, el programa tratará de traducirlo a las
estructuras convenientes para un libro electrónico. **Ya pueden
imaginarse lo desastroso que será el proceso cuando se hacen
cambios de diseño a diestra y siniestra**…

¿Quieres convertir tus libros sin ayuda y que al menos parezcan
aceptables? Todo comienza en el uso sistemático de los estilos
de párrafo o de caracteres que nos ofrecen los programas que
utilizamos para editar o maquetar nuestra obra. No importa cuál
_software_ utilices, todos tienen esta posibilidad.

## Desarrollo: en las entrañas del _ebook_

Si ya has pasado por la conversión de _ebooks_ pero te has frustrado
en el intento, no es porque la producción de _ebook_ sea un proceso
de otro mundo. **La dificultad de producir un libro electrónico
como uno quiere se debe casi siempre a fallas metodológicas**.

En los formatos impresos estamos acostumbrados a trabajar a partir
del diseño. Es decir, _tal como vemos la obra esperamos que
igual sea su resultado_. Por eso, la conversión se encarga
de «traducir» su diseño a estructuras y hojas de estilos.

Para el caso de los libros electrónicos el enfoque es inverso.
**En un _ebook_ las estructuras condicionan al diseño**. Un libro
electrónico limpio, ligero y lindo ---desde ahora las llamaré
las «tres eles»--- se obtiene cuando se cuida el código que está
de fondo. Con esa consistencia y simplificación es muy sencillo
dar órdenes uniformes sobre cómo mostrar la publicación.

Los medios para conseguir este control y resultados tienen una
regla muy sencilla: la publicación no se convierte en su formato
final sino, a lo sumo, a un formato intermedio.

Sigamos con el ejemplo de los documentos de Word o de InDesign.
En lugar de convertirlos a +++EPUB+++, se convierten a +++HTML+++.
A continuación, con algún programa ---el más popular es [Sigil](https://sigil-ebook.com)---
se empiezan a trabajar el resto de los archivos que comprenden
un libro electrónico para al final generarlo.

### Ventajas y desventajas

Esta manera de trabajar tiene **ventajas muy claras**:

- Mayor control.
- Más uniformidad.
- Mayor calidad editorial.
- El producto final es de tres eles.

Pero también tiene **un par de desventajas**:

- Mayor tiempo de producción.
- Bifurcación del proyecto o esperar el cierre de la edición del libro
  impreso.

Para una persona que viene de un enfoque visual de producción
de libros, hay **otras desventajas**:

- Se requieren conocimientos de, al menos, +++HTML+++ y +++CSS+++.
- Implica trabajar pensando en las estructuras y luego en el diseño.
- Se emplean diversas herramientas ajenas al diseño editorial.

Por estos motivos, es más común ver que este tipo de trabajo
sean desempeñados por proveedores externos. **Entre conversores
o desarrolladores de _ebooks_, los últimos cobran más por sus
servicios, porque son más especializados**.

¿Quieres producir libros electrónicos con la mayor calidad posible?
Vale la pena empezar a ver a las publicaciones como estructuras
y a conocer lo que comprende las entrañas de un +++EPUB+++.

## Recomendación: qué quiero y qué puedo esperar

Como desarrolladores de +++EPUB+++, tendemos a cometer el error
de pensar que todos los clientes esperan el mejor resultado posible.
En varias ocasiones he tropezado con que el cliente lo único
que quiere es leer su obra en su dispositivo. Entonces, **¿qué
conviene más: la conversión o el desarrollo?**

**Si tus necesidades son personales** o no van más allá de un
círculo cercano; en definitiva, **lo tuyo es** hacer **la conversión
con tus propios medios**. Sea con InDesign, Calibre u otra plataforma;
lo que te interesa es tener el archivo lo más pronto y barato
posible.

Puede darse el caso de que quieres ofrecer tu obra más allá de
tu círculo más cercano; también **para cualquier internauta,
pero tienes muy poco presupuesto**.

En este caso, **lo recomendable es** elegir a alguien que te
ofrezca **un servicio de conversión**, que sea esta persona quien
lidie con los metadatos, el diseño y la validación técnica.

**Si representas una editorial** donde el libro electrónico hablará
mucho de tu sello, querrás ofrecerle a tu lector el formato con
la mejor calidad y diseño posible.

En este sentido, **la recomendación es** tener un proveedor confiable
que te dé **un servicio de desarrollo**, no de conversión. De
esta manera, existe la seguridad de poder tener publicaciones
con diseños personalizados y uniformes, más si se trata de colecciones.

No obstante, **si lo que te interesa es vivir de esto o que todos
los procesos de publicación digital se lleven a cabo de manera
interna, no hay otro camino sino aprender las entrañas de la
publicación electrónica**.

No importa cuántas capacitaciones o talleres se tomen. Lo primero
que se debe hacer es aprender las estructuras internas de cada
uno de los formatos finales que se desean.

Complicado, ¿cierto? Sin embargo, todo se simplifica cuando se
concibe que **tanto el formato para impreso como el digital pueden
partir de la estructura al diseño**, con lo que se evitan esperas
o bifucarciones. En su lugar, se abre el panorama para empezar
a concebir a la edición como automatización y multiformato. Pero
esto es [otra historia](@index[8])…

<aside class="espacio-arriba2">

###### Sobre esta entrada

* Título original: «¿Qué son la conversión y el desarrollo de _ebooks_?».
* Título en el _blog_ de Mariana: «Cuál es la diferencia entre la conversión de _ebooks_ y desarrollo de _ebooks_».
* Fecha de publicación: 28 de febrero del 2019.
* [Redacción original](https://gitlab.com/NikaZhenya/entradas-eguaras/blob/master/27%20-%20%C2%BFQu%C3%A9%20son%20la%20conversi%C3%B3n%20y%20el%20desarrollo%20de%20ebooks%3F/entrada.md).
* [Revisión de Mariana](https://marianaeguaras.com/cual-es-la-diferencia-entre-la-conversion-de-ebooks-y-desarrollo-de-ebooks/).

</aside>

