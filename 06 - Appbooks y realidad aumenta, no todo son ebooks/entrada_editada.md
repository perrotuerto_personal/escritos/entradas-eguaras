# 6. Appbooks y realidad aumentada, ¡no todo son ebooks!

Las posibilidades tecnológicas actuales permiten una clase de
publicaciones cuya metodología poco tiene que ver con la tradición
editorial. Esta esencial diferencia se debe a que se trata de libros
electrónicos que sobresalen por su alta interactividad o propuestas que
no se ciñen al paradigma de la página, la lectura lineal de una obra y
las narrativas que permite este soporte. Este tipo de **productos
editoriales** es lo que últimamente se conoce como **_appbooks_**.

Este término proviene principalmente por la popularización de las
tiendas de aplicaciones como Google Play o App Store. Sin embargo, **los
_appbooks_ anteceden a los teléfonos inteligentes y las tabletas**.

De hecho, tienen su antecedente cuando se empezó a experimentar con la
escritura a través de la computadora y cuyos resultados no se pensaron
para la impresión.

Por este motivo, los _appbooks_ han existido no solo como aplicaciones
para iOS o Android, sino también en forma de +++CD+++ interactivos (¿quién no
recuerda la enciclopedia interactiva de Salvat?), plataformas _web_,
videojuegos o aplicaciones con realidad aumentada.

## Metodologías de desarrollo variable según el tipo de narrativa buscada

Para los _appbooks_ no existe un programa especialmente diseñado para su
creación. En su lugar, se utilizan una serie de diferentes tecnologías:

* **Páginas +++HTML+++ estáticas** en los que únicamente se usa +++HTML+++, 
  +++CSS+++ y JavaScript. Estas son una de las propuestas más tempranas 
  que se basaron, principalmente, en la narrativa hipertextual (donde 
  la lectura no tiene un seguimiento lineal, sino que da «saltos» a
  través de enlaces _web_). Aunque no se reduce únicamente a esto,
  sino también al empleo de programación para cambiar de manera
  dinámica el texto o desatar otros eventos, como en
  [CLIteratura](https://cliteratu.re/).
* **Aplicaciones _web_** que interactúan con un servidor para obtener
  información. La originalidad narrativa de estas aplicaciones tiende
  a ser su manera de organizar una gran cantidad de información
  textual. Por lo que en general se tratan de diccionarios, como
  [_Stanford Encyclopedia of Philosophy_](https://plato.stanford.edu/); 
  enciclopedias, como Wikipedia; o catálogos, como el que en Nieve de 
  Chamoy estamos creando para las obras del [Museo Soumaya](http://www.soumaya.com.mx/index.php/esp).
* **Aplicaciones híbridas** en las que se mezclan elementos _web_ con
  el uso del _hardware_ del dispositivo móvil. Esto hace posible que
  el _appbook_ no tenga que abrirse desde un explorador, sino que esté
  alojado en el móvil como una aplicación independiente. En este, la
  narratividad aún está centrada en lo textual, al menos al momento de
  organizar la información, por lo que el texto se estructura de tal
  manera que es sencillo leer y navegar a través de tantos datos.
  Ejemplos son la _app_ de Jazz at Lincoln Center o la
  aplicación que hicimos sobre los cien años del Futbol Club Atlas.
* **Aplicaciones nativas** que utilizan directamente las posibilidades
  que ofrece el dispositivo. Estas aplicaciones se desarrollan así
  para obtener una gran optimización para una narrativa interactiva,
  como _Our Choice_ o la [_Guía del Prado_](http://www.laguiadelprado.com/index.html).
* **_Software_ creado con motores de videojuegos**. Las posibilidades
  narrativas se aproximan más a la *jugabilidad* y, por lo general, ya
  contemplan ciertas características habituales de los videojuegos,
  como un sistema de puntuación o la interacción con personajes
  virtuales. Esto es perceptible en el trabajo que realiza
  [Concretoons](http://cartuchera.concretoons.com/index.html), los
  videojuegos independientes como [_The Mammoth_](https://inbetweengames.itch.io/mammoth-download) 
  o la aplicación _En busca de Kayla_ que desarrollamos para la editorial 
  Sexto Piso.
* **_Software_ que involucra [realidad
  aumentada](https://es.wikipedia.org/wiki/Realidad_aumentada).** Esta
  clase de _appbook_ busca alimentar la narrativa textual de una
  publicación impresa. Ofrece elementos multimedia que se desatan
  cuando se enfoca la cámara del dispositivo sobre la obra, como [_La
  leyenda del ladrón_](http://www.pitboxmedia.com/la-leyenda-del-ladron-libro-con-realidad-aumentada/)
  o la que desarrollamos para la [Universidad Nacional Autónoma de
  México](https://itunes.apple.com/mx/app/unam-futuro/id1042823048?mt=8).

## Limitaciones temporales, técnicas y económicas

En cualquier ámbito profesional o institucional de trabajo siempre
tenemos el problema de que, por lo general, el tiempo destinado a un
proyecto no es el deseado para el resultado más óptimo. Algunos detalles
no son perceptibles para el público en general; por ejemplo, los
callejones blancos de un impreso, que sí tienden a ser vistos entre
editores. Es decir, muchas veces esos _errores_ no afectan al uso
general de un producto.

Sin embargo, **en el desarrollo de _appbooks_ los descuidos son lo
primero que advierte el usuario**. ¿Cuántas veces ha molestado usar una
aplicación cuya navegación no es «amistosa» o hace cosas «inesperadas»?

Los detalles que pueden encontrarse en un _appbook_ suelen afectar a la
navegación general de la obra, lo que puede llevar a una evaluación
negativa o devolución del producto.

En la creación de _appbooks_ ha de tenerse presente que estos detalles
estarán presentes si el tiempo es escaso. Esto es relevante si la
publicación es para un tercero, ya que se ha de indicar para evitar
futuros problemas.

En este sentido, es ideal que los _appbooks_ cuenten con tiempos de
pruebas y de optimización según los dispositivos de salida. Estos
tiempos pueden ser de algunas semanas a varios meses, según la
complejidad de la publicación.

«Fase de pruebas» y «fase de optimización» no son procesos comunes
dentro de la producción del libro: **los _appbooks_ tienen una
metodología más afín al desarrollo de _software_ o de videojuegos**. Por
lo general, esto también implica limitaciones:

* quienes editan carecen de conocimiento para el desarrollo de _software_, 
  por lo que el _appbook_ presenta problemas de desempeño, o
* quienes programan desconocen el control de la calidad editorial,
  ocasionando que el _appbook_ tenga una baja calidad en la edición.

La **recomendación** para los que desean crear este tipo de
publicaciones es que, en un primer momento, se decidan por un tipo de
_appbook_ de los que se mencionaron en la sección anterior y que, de
manera paulatina, se vaya aumentado el grado de complejidad. Pero, y
principalmente, que se tenga en cuenta que la inclusión de un artista
digital y alguien que programe es indispensable.

Y considerar que cuando se realiza una contratación externa de estos
servicios existe la posibilidad de una falta de comunicación, ya que la
jerga editorial les es desconocida, así como los conceptos fundamentales
para el desarrollo de _software_ o del arte digital no forman parte de
la cultura general de quien edita, lo que puede ir en detrimento de la
calidad.

Estas limitaciones de tiempo y de técnica acarrean que los costos de
producción de un _appbook_ no sea el que se piensa en un principio, más
si no existe una planificación adecuada. ¡Se está desarrollando
_software_!

**El costo de creación de _appbooks_ es igual o mayor al de un
impreso**. Si se piensa que el desarrollo de un _appbook_ implica
tiempos y esfuerzos similares al desarrollo de un libro electrónico
estándar (+++EPUB+++, +++MOBI+++ o +++IBOOKS+++) es mejor que este entusiasmo se guarde
para un proyecto muy importante…

## El lugar actual de los _appbooks_ en el mundo editorial

Por la disparidad metodológica y las diversas limitantes que implica el
desarrollo de aplicaciones, **los _appbooks_ no han llegado para
reemplazar a los libros**, ni a los _ebooks_, al menos no durante los
siguientes cinco o diez años. El lugar de esta clase de publicaciones no
es dentro del grueso de la producción de libros, sino de excepciones
dentro del mundo editorial.

Cuidado, que este carácter marginal no nos engañe. Esto no quiere decir
que su potencial comercial sea escaso. Al contrario, un _appbook_ bien
desarrollado y publicitado puede fácilmente transformarse en un
contenido viral que genere millones de descargas.

Sin embargo, una alta calidad en el desarrollo y una buena estrategia de
_marketing_ digital, orientada a la venta de aplicaciones, no son campos
dominados por el sector editorial. El sector del desarrollo de
_software_ tampoco tiene un completo dominio, ya que en su mayoría se
orientan a la creación de sistemas y soluciones empresariales, no a la
creación de aplicaciones interactivas para el público general.

**Los estudios de videojuegos** son los que **tienen una mayor ventaja
para el desarrollo de _appbooks_**, debido a que son los que cuentan con
un alto dominio y experiencia de las características técnicas y de las
narrativas interactivas necesarias para interesar al usuario. De hecho,
esto queda fácilmente demostrado al ver quién está detrás de nuestros
_appbooks_ favoritos.

Entonces, ¿qué hacemos en la industria editorial con los _appbooks_?
¡Experimentar! **Los _appbooks_ pueden ser una muestra sobre cómo será
posible «deconstruir» la lectura** y, por ende, una evidencia de los
profundos cambios que representan las publicaciones digitales para los
procesos editoriales.

Esto se debe a que recuperar la inversión necesaria para el desarrollo
de un _appbook_ es aún más difícil que agotar una edición de un impreso.
Por ello, es mejor tomar el proyecto con cierto pesimismo y partir del
supuesto de que la inversión no se recuperará.

O bien, crear esta clase de publicaciones si la entidad editorial cuenta
con la posibilidad  «orgánica» de producción, donde todo se crea
internamente con una clara idea técnica y un calendario realista.

Otra opción, que también implica tener una estructura orgánica, es
ofrecer los _appbooks_ como un servicio editorial a instituciones
públicas o privadas, para ir ganando experiencia. Una destreza que
servirá al momento de producir _appbooks_ bajo nuestro sello editorial.

Por otro lado, en el mundo editorial todavía es muy difícil separar la
idea de «obra» a la de «libro», más cuando este último término quiere
decir «libro impreso». La asociación había sido tan normal que el simple
hecho de hablar de diseño fluido, como es posible en libros electrónicos
estándar (+++EPUB+++, +++IBOOKS+++, +++MOBI+++), ya es una noción difícil de entender
porque cambia la idea de lo que se entiende por «página». A esta
complicación sumemos que en muchos _appbooks_ la idea de «página» no es
difusa sino inexistente…

## Paradigma de edición para _appbooks_: ¿páginas o dinámicas?

Ejemplos de esta dificultad los tenemos en Nieve de Chamoy todos los
días. Cada vez que hacemos una aplicación para un cliente ---en muchos
casos profesionales de la edición---, lo primero a esclarecer es que
**en el desarrollo de los _appbooks_ es indiferente la cantidad de
páginas**. Para su creación lo relevante es la cantidad de dinámicas que
se podrán hacer, los recursos que se tienen y la cantidad de imágenes,
videos o audios a incluir (e incluso también el formato de origen de los
contenidos).

La complejidad no es proporcional a la cantidad de texto, como lo es en
la corrección o la traducción. La «página», la «cuartilla» o las
cantidades de palabras o de caracteres son tan fundamentales para
calcular los costos en el mundo editorial que, por lo general, la
creación de un presupuesto para _appbooks_ ya es una causa de conflicto.

Además, si entre quienes editamos la publicación de libros electrónicos
estándar es un reto, mayores dificultades enfrentamos en el desarrollo
de _appbooks_. Si entre los profesionales de la edición aún es muy
difícil desembarazar las ideas de «obra» o «libro» de la de «impreso», o
de que **la página ya no es el único paradigma para la edición**, ya
podemos imaginarnos lo complicado que esto puede ser para el lector en
general que, sin dudas, estimará o rechazará la propuesta narrativa de
un _appbook_ dentro de un universo digital donde se convive con las
películas y los videojuegos.

Para finalizar, comparto una pequeña lista de reproducción de _appbooks_
de diversa índole. Este material lo empleamos en talleres, seminarios o
pláticas. Si conocen un video que sea relevante para esta lista, por
favor, deja constancia de él en los comentarios.

<aside class="espacio-arriba2">

###### Sobre esta entrada

* Título original: «_Appbooks_ y realidad aumenta, ¡no todo son _ebooks_!».
* Título en el _blog_ de Mariana: «Appbooks y realidad aumentada. ¡No todo son ebooks!».
* Fecha de publicación: 22 de marzo del 2017.
* [Redacción original](https://gitlab.com/NikaZhenya/entradas-eguaras/blob/master/06%20-%20Appbooks%20y%20realidad%20aumenta,%20no%20todo%20son%20ebooks/entrada.md).
* [Revisión de Mariana](https://marianaeguaras.com/appbooks-y-realidad-aumentada-no-todo-son-ebooks/).

</aside>
