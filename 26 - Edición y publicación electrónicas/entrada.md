# Edición y publicación electrónicas

La edición y la publicación son dos términos muy emparentados que por lo
general usamos como sinónimos. La mayoría de los casos esto no causa ningún
problema. Sin embargo, hay veces que sí es necesario tener presente que
se trata de cosas distintas.

Para esta entrada retomo un poco [un artículo](https://marianaeguaras.com/editar-y-publicar-no-son-sinonimos)
publicado por Mariana. Pero desde mi principal contexto profesional: el
desarrollo de publicaciones digitales.

## Edición electrónica

Con la edición buscamos producir un objeto a partir del material textual y
gráfico que nos da el autor o el cliente. En general se trata de una serie de
procesos para dar con una salida legible y accesible al lector. Esto implica
que existe una diferencia cualitativa entre el material de base y los formatos
finales. __El quehacer editorial existe para mejorar la calidad de la obra__.

Gran parte de los procesos editoriales ocurren tras bambalinas. En muchas
ocasiones se dan por supuestos. En efecto la edición es una profesión, un arte
y una tradición. Pero también es un método que implica la ejecución de
procedimientos. Este se hace muy claro en un contexto de edición digital en
el que se buscan múltiples formatos finales.

¿Qué es la edición digital? No es, como podría pensarse, el _ebook_. Por
supuesto la edición digital _produce_ libros electrónicos. Sin embargo, la
edición es un proceso, no un producto. __La edición electrónica es el desarrollo
de formatos —+++PDF+++, +++EPUB+++, etcétera— a través de una computadora.__

Si usas una computadora para hacer publicaciones, no importa los programas o
procesos que utilices, es ya edición digital. No se trata con tinta o acetatos
y papel, sino con bits que se muestran como fuentes y pixeles y que quizá una
impresora los traduzca en material tangible.

Este desarrollo de formatos en sí no implica su publicación. Al editar a través
de una computadora no necesariamente el material sale al público. Hasta este
punto aún nos encontramos tras bambalinas. Solo el editor, los colaboradores y
el autor o el cliente tienen conocimiento de que la obra _se está editando_.
Es decir, que la publicación está en proceso de mejorar su calidad antes de su
disposición al público.

## Publicación electrónica como proceso

Una vez que los involucrados en la edición de una obra dan su visto bueno, puede
procederse a su disposición pública. En un contexto de publicación impresa
el procedimiento empieza en la preprensa, pasando por su impresión distribución
y hasta su comercialización en los puntos de venta.

__La edición es la gestación de la obra detrás de un muro; la publicación como
proceso es la salto de esa muralla al exterior__. Las fases habituales implican
que la obra, al publicarse, se «abre» al lector.

__¿Qué es la publicación electrónica como proceso? La disposición pública del
_ebook_ en alguna plataforma__. El procedimiento implica la subida de archivos
a sitios como Amazon, iTunes o Google Play para que el usuario pueda adquirirlos
y leerlos.

Este proceso puede llegar a ser un dolor de cabeza. Si bien las tres plataformas
mencionadas con anterioridad se llevan la mayoría de la cuota del mercado,
existen cientos de sitios en donde es posible comercializar la obra. Para
facilitar la tarea, existen servicios de distribución donde es posible llevar
la publicación a centenares de tiendes electrónicas desde una sola plataforma,
por ejemplo, [Bookwire](https://www.bookwire.de/es).

## Publicación electrónica como producto

La publicación también se usa para señalar a un objeto. En la tradición
editorial la publicación se trataba de un impreso. Ahora también es el archivo
que el lector lee desde un dispositivo electrónico.

Por ello, __la publicación electrónica como producto es un formato que solo
puede usarse a través de un dispositivo__. El impreso se trata de un objeto
tangible que tomamos con nuestras manos. La publicación digital es un archivo
que únicamente puede leerse desde una computadora, tableta o teléfono
inteligente.

En el advenimiento de la edición digital fueron estos nuevos formatos los que
se llevaron el foco de atención. En la actualidad lo más relevante de la
«revolución» digital es el cambio que ha generado en la infraestructura y
procesos para producir una publicación.

## Edición y publicación electrónicas como servicio

La transformación de la industria editorial está en su comienzo. Décadas atrás
de su surgimiento y experimentación están abriendo paso a procedimientos cada
vez más maduros. Sin embargo, aún estamos lejos en volver a tener el control
de los principales procesos, como ya se tenían en el contexto análogo.

En esta situación, como editores, autores o clientes podemos sentirnos un tanto
abrumados. En diversos espacios dedicados a la edición y la publicación vemos
la gran cantidad de ofertas para poder pasar de nuestra materia prima a una
obra con alta calidad editorial.

Ahí, la edición y la publicación ya no son solo tratadas como procesos o
producto. __El contexto digital ha permitido la proliferación de la edición y la
publicación como servicio__.

No es que antes fuera inexistente. Las ediciones por encargo son una buena
muestra de ello. La diferencia radical es que __en la edición y la publicación
electrónicas es cada vez más común que profesionales de la edición ofrezcan sus
servicios__. La organización tradicional de un autor que manda un texto para ser
dictaminado, editado y publicado no es ya el único ecosistema.

Poco a poco los ingresos bajo concepto de «servicios» empiezan a generar un
ecosistema editorial distinto. Antes el autor no absorbía el riesgo en la
inversión, por lo que a cambio cedía sus derechos al editor —la relación
«tradicional» autor-editor—. Ahora el autor paga al profesional de la edición,
mientras que este realiza un servicio del cual el autor no pierde los derechos.

Al menos eso se supone… La popularización de este ecosistema en un imaginario
donde persiste la relación tradicional autor-editor genera más de un problema.
En buscadores de internet o redes sociales diversos autores encuentran un sin
fin de ofertas de servicios editoriales que les prometen hacer de su obra un
_bestseller_.

Como la mayoría de los casos los autores nóveles desconocen los ecosistemas
editoriales, es común que varias de esas ofertas sean apócrifas. Las estafas
pueden ser de varias maneras, la «editorial»:

* utiliza un nombre semejante a una editorial de renombre para simular que el
  autor será parte del catálogo de la editorial original;
* cobra una suma irreal e intenta convencer al autor de que es el costo estándar
  por editar y publicar;
* presume su alta calidad editorial cuando en realidad publican la obra con un
  deficiente cuidado, si lo hay;
* promete que la obra estará en librerías y cobra por la impresión de miles de
  ejemplares, cuando en realidad imprime unas decenas que nunca se distribuyen;
* asegura fama al autor pero simula las relaciones públicas habituales como
  presentaciones de libros, entrevistas o promoción de la obra, o
* garantiza que el trabajo se realizará en un par de semanas y pide un pago
  completo para luego desaparecer.

La manera de evitarlo es exigir claridad al posible editor de tu obra sobre los
precios, los tiempos, los servicios que prestará y en quién tendrá los derechos.

Mariana es una de las editoras que como autor necesitas. Para satisfacer todas
tus necesidades, hemos habilitado un [nuevo servicio](https://marianaeguaras.com/ebooks/)
para ofrecerte la edición y la publicación electrónica de tu _ebook_. Ahora
ya es posible desarrollar tu obra en formato estándar +++EPUB+++ para todos los
distribuidores y en +++MOBI+++ para ser descargado directamente a dispositivos
Kindle, así como tendrás disponible las estadísticas de tu obra —como
[este ejemplo](http://ed.cliteratu.re/ebooks/recursos/epub-automata/logs/pc-analytics/analytics.html)—.
