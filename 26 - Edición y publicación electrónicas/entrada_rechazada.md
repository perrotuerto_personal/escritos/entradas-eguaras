# Edición y publicación electrónicas

La edición y la publicación son dos términos muy emparentados que por lo
general usamos como sinónimos. La mayoría de los casos esto no causa ningún
problema. Sin embargo, hay veces que sí es necesario tener presente que
se trata de cosas distintas.

Esta entrada surge por tres motivos donde he notado que la diferencia es
importante. En la pasada +++FIL+++ de Guadalajara observé un empuje
fuerte hacia la idea de publicar, pero casi no observé una reflexión sobre qué
es editar. De manera constante en el trabajo Melisa y yo tenemos que señalar
que no es lo mismo la edición digital a la publicación digital. Por último,
existe por ahí un proyecto de lenta cocción en donde estoy traduciendo algunas
entradas publicadas aquí a «_broken_» _english_. Una de las primeras
dificultades es que al parecer el uso de la palabra «_edit_» es más genérico
al que nosotros empleamos con «editar» y que allá es más común hablar de
_publishing_ a lo que nosotros tratamos como edición.

El contexto por el que hablo es principalmente desde mi trabajo desarrollando
publicaciones electrónicas. Con seguridad Mariana u otras editoras con
experiencia en impreso pueden enriquecer o refutar el contenido de esta
entrada. También existe por ahí una discusión si «electrónico» es lo mismo a
«digital». Por el momento la obviaré.

## Edición electrónica/digital

La edición es un proceso cuya finalidad es producir un objeto —_output_— a
partir de material textual o visual hecho por el autor —_input_—. Ese objeto
bien puede comercializarse o distribuirse de manera gratuita. Pero en
definitiva requiere el trabajo para procesar los _inputs_ en un _output_ que
sea legible y accesible al lector.

Antes de publicar una obra, esta tiene que «curarse». __El quehacer editorial
implica una transformación de la materia prima a una herramienta que pueda
ser usada por el lector__. Si el _output_ no es el mismo al _input_, estamos
hablando ya de la existencia de un proceso editorial, por más rupestre que
este sea.

Gran parte de los procesos editoriales se dan tras bambalinas. Muchas veces
el autor no tiene conocimiento pleno de lo que esto implica. Pero también es
común que el editor los desconozca. La edición es una profesión, un arte y
una tradición. No importa qué tanto nos guste enaltecer lo que hacemos, al
final la «pragmática» editorial es la ejecución de procesos. Sobre los
procedimientos reflexionamos o robustecemos —«semántica» editorial— y nos damos
cuenta que no es una mera ejecución maquinal. De ahí el objeto sublime que es
el libro. Aunque en varias ocasiones ya lo damos como dado y de antemano 
esperamos un conjunto de relaciones habituales para llevar a cabo un proyecto
editorial —«sintaxis» editorial—.

__En varias ocasiones se da tan supuesto los procesos que hasta el editor
no sabe lo que implican__. Y dentro de los distintos tipos de la edición,
considero que la edición digital evidencia muy bien ese problema.

¿Qué es la edición digital? En talleres y charlas es común que nos respondan
a partir del _output_ que según se consigue: el _ebook_. Por supuesto que la
edición digital _produce_ libros electrónicos. Sin embargo, la edición digital
no _es_ la publicación digital. Primero, porque editar es un proceso, no un
producto. Segundo, debido a que los procesos de la edición electrónica no solo
es para obtener _ebooks_, sino también impresos.

__La edición electrónica es conseguir los _outputs_ necesarios —impresos o
digitales— a partir de procesos y de _inputs_ que no son tangibles__. Si
usas una computadora para hacer o escribir publicaciones, no importa el programa
o procesos que utilices, es ya edición digital. No tratas con tinta o
acetatos y papel, sino con bits renderizados en fuentes o pixeles y pantallas.
Si tu obra será impreso, la tangibilidad se consigue a través de su publicación.

## Publicación electrónica/digital como producto

Aunque la publicación es un proceso, la mayoría de las personas la entienden
como un producto. No solo entre lectores, sino también entre editores e
impresores. __Al final, ¿cómo es posible hablar de edición sin publicaciones?__

El _output_ anticipado en la edición es un objeto. Por lo general uno que
pueda asirse o descargarse. La relevancia al producto es tal que en la
actualidad vemos una y otra vez la importancia de la edición pero como aquello
que permite la producción de muchos o de sublimes objetos. La importancia al
producto es tan grande que la edición hoy por hoy tiende a reducirse a una
economía productivista.

Hasta la educación continua a través de seminarios, talleres o congresos por
lo general demuestra que lo que a muchos editores en este tiempo les importa
es producir. __Sea mejor o a mayor escala, estamos en una situación en donde la
edición se define principalmente a partir de su _output_, quedando relegados o
subordinados los _inputs_ y los procesos__.

De nueva cuenta insisto en que la edición digital da un buen testimonio. Durante
varias décadas se pensó que lo más destacado de la «revolución digital» en la
edición era el advenimiento de nuevos formatos. Hasta vino a colación para ese
famoso debate sobre la muerte del impreso. Y pasaron los años y más que
conflicto, se ha observado que son formatos que han venido a satisfacer
distintos intereses. Incluso visto como ecosistema, entre formatos se fomenta
el alza en las ventas.

__La publicación electrónica como producto es un _output_ que solo puede asirse
a través de un dispositivo digital__. La publicación impresa se toma con las
manos directamente. Esa diferencia más la predilección por privilegiar el
producto produjo el supuesto que lo más relevante de la edición digital fueron
los nuevos formatos.

En un ámbito de _inputs_ y procesos esto alimenta la idea de que el _output_
electrónico requiere de procedimientos muy distintos al del impreso. Cierto es
que cada formato tiene sus particularidades. No obstante, en la publicación
estándar esta diferencia no es tal que justifique una bifurcación metodológica.

No se necesitan dos procesos completamente distintos para producir dos formatos:
el _ebook_ y el impreso. Incluso __es un error metodológico suponer que una
diversidad de _outputs_ requiere una multiplicidad de métodos__. Esto no es un
simple equívoco. En el editor genera la idea de que en todo momento y desde
ahora ya requerirá de otros profesionales de la informática para poder llevar a
cabo su trabajo. En el plano metodológico genera el conflicto presente en la
edición cíclica: más tiempo, más recursos y menos calidad por cada nuevo formato
requerido.

La publicación digital como producto no requiere de métodos aislados para
desarrollarlos a la par del impreso. Lo que sí necesita es una metodología cuya
finalidad sea la consecución de varios _outputs_ y no solo uno. __En mi opinión
eso es lo más importante en la edición digital: ha abierto un nuevo panorama
para poder producir mejores obras multiformato__. Pero eso pueden verlo en
[otra entrada](http://marianaeguaras.com/edicion-ciclica-y-edicion-ramificada-de-la-antesala-a-la-revolucion-digital)
en la que se habla sobre otro modo de editar: la edición ramificada.

## Publicación electrónica/digital como proceso

La publicación como proceso es la parte más truculenta. Si la edición es
en principio un proceso y la publicación por lo general se entiende como un
producto, ¿dónde queda la publicación como proceso? Una respuesta podría ser
que se tratan de dos procedimientos bien delimitados. Sin embargo, en varios de
los casos la publicación se percibe como una etapa más del quehacer editorial.

Pienso que es posible concebirlo como un proceso dentro de la edición al mismo
tiempo que es un procedimiento que va más allá del editor. Pero solo es así
si suponemos que el trabajo principal del editor es, vaya al redundancia,
editar.

__Por lo general pasamos por alto que un editor no solo edita, también comercia,
cabildea, hace publicidad y da atención al cliente__. Si hacemos de lado grandes
consorcios editoriales, son pocos los editores que pueden dedicarse de manera
exclusiva al sublime arte de editar. Esto es de particularidad interés por la
precariedad económica o elitismo que explicita. Pero lo dejaré de lado para
concentrarme en los _inputs_ y los _outputs_.

Considero que __la publicación como proceso tiene como características el
dar a luz al menos un _output_ y el ser un elemento que está en la esfera
pública__. Podemos editar todo lo que queramos, pero si no publicamos nunca
tendremos un _output_ para el lector, ¿cuántas obras no hemos editado que
por uno u otro motivo nunca han sido publicadas?

La referencia a la esfera pública puede ser muy discutida. La publicación es
un proceso que muchos pueden ver, empezando por el impresor, pasando por el
distribuidor, hasta el lector que lee la obra pero no la adquiere o los medios
de comunicación. Al hacer pública una obra el editor empieza a perder el
control que tanto tuvo en los procesos de edición. Sin embargo, __la mayoría
de las veces la publicación no es un proceso sujeto a discusión pública__.
Muchos de nosotros, como lectores, poca injerencia tenemos sobre lo que se
publica más allá de boicot comerciales.

__La edición es un proceso que, por así decirlo, está detrás de un muro__. Sea
la revisión peer-to-peer o los colaboradores en una edición de diversa índole,
es común que la edición sea un proceso solo accesible a unos cuantos. En la
publicación la obra «se abre» no solo a los que contribuyeron a generarla.
Existen ejercicios interesantes para destruir esta muralla, como la Wikipedia
o las traducciones colaborativas. Sin embargo, aunque sea posible que todos
editemos, en los hechos se restringen a ciertos grupos. Un motivo puede ser
la reproducción de centros de poder que alejan a nuevos editores, otro es que
simplemente hay más personas que prefieren leer o escribir que editar.

En mi opinión, el tipo de publicación que ha hecho más evidente que se trata
de un proceso dentro de la esfera pública es el movimiento del acceso abierto
en la academia. Estas personas han explicitado los peligros que conlleva para
la producción del conocimiento que la publicación esté en manos de medios que
solo permiten su uso a través de un muro de pago.

__La publicación electrónica como proceso es la colocación de los _outputs_ en
plataformas digitales__. En la academia las plataformas hegemónicas como
Elsevier o +++JSTOR+++ implican una cesión de derechos por los cuales las
instituciones y los investigadores pierden mucha capacidad de maniobra. Fuera
de ese contexto, son Amazon, iTunes y Google Play quienes se llevan la mejor
tajada. Y aunque este modo de publicación implica la eliminación de varios
intermediarios, asimismo acarrea el problema de nuestra dependencia a las
políticas cambiantes de esas plataformas comerciales, en lugar del
proteccionismo legislativo al que el editor estaba acostumbrado.

## ¿Publicación antes que edición?

Hay varios elementos dejados al aire: «sintaxis», «semántica» y «pragmática»
editoriales, ediciones ramificada o cíclica, así como de _inputs_ y _outputs_.
También concepciones que requieren de reflexión como la edición a modo de un
muro o el editor con más responsabilidades que las de editar.

__Esto permite entrever que en la actualidad ya no tenemos una idea general de
lo que es editar y sus implicaciones__. Pero se trata de una preocupación que
ha sido subordinada a los intereses económicos que permiten los _outputs_ en
disposición pública.

No se trata de una posición conservadora en la que quiera alarmar a los demás
sobre por qué no deberíamos de dar tanto empuje a la publicación en lugar de
concentrarnos en la reflexión sobre la edición. Al final el editor primero
actúa y luego piensa en torno a su acción. No obstante, como fue perceptible
en la +++FIL+++ de Guadalajara, ¿estamos seguros de que lo más pertinente para
nuestro ecosistema es abalanzarnos en el sin fin de opciones comerciales que
se nos ofrecen para poder llevar a cabo nuestro trabajo?

Para decirlo con otros términos: __la publicación se ha privatizado__. Los
puntos de distribución actuales nos obligan a ceder derechos, aceptar rebajas,
normalizar cuentas ofuscadas o negociar para demostrar que somos los propios
productores de nuestras obras. Los procesos de publicación hace ya tiempo que
dejaron estar bajo la regulación de una entidad Estatal. Quizá la publicación
nunca fue una cuestión completamente pública, pero ese polo tenía mucho peso:
el libro no era trato principalmente como mercancía.

En la edición también existe esa tendencia a la privatización de los procesos,
modos de producción y conocimientos. __la edición ha sido considerada un
objeto que le pertenece a un gremio__ y no como procesos abiertos para
cualquiera que quiera publicar. Sin embargo, veo ahí mucho potencial no solo
para descentralizar y tirar los muros de la edición, sino también para hacer
de esta una cuestión que nos atañe a todos.

Mientras la edición sea reducida a programas de cómputo (privados), ese
peligro a su privatización está latente. ¿En realidad queremos darle preferencia
a un modo de editar y de publicar que sangra nuestra economía por cualquier
pretexto? ¿Es eso lo que queremos, ser profesionales (precarios) en competencia,
en lugar de estar en convivencia? Pero también, __¿de dónde viene ese supuesto
de que el quehacer editorial _es_ la producción masiva o estética de objetos?__

> Rechazada por considerarse muy compleja para un _blog_.
