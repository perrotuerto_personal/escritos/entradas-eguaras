# 23. Un giro digital en la edición y una propuesta sobre metodología editorial

¿Qué alternativas tenemos ante un panorama lleno de amargos desencantos
entre la tradición editorial y las nuevas tecnologías de la información
y la comunicación?

¿Qué otros planteamientos son posibles además de los comunes contratos
entre entidades editoriales y empresas prestadores de servicios de
*software*?

¿Cómo los editores estamos destinados a que la calidad ya siempre
dependa de un intercambio económico por los conocimientos técnicos de
terceros?

## Origen de los problemas

La edición se hizo código. En otras palabras: el quehacer editorial ya
no es disociable del desarrollo de *software*. El texto, como lo
habíamos comprendido desde la invención de la imprenta, tiene nuevas
dimensiones que aún no terminamos de entender del todo.

Una publicación no solo tiene tras de sí una tradición relativa a los
procesos editoriales y gráficos. El traslado de la manera de trabajar a
una computadora no es un cambio de tecnologías o de técnicas: es un
**cambio de método**.

¿Cansado de que cada vez es más común exigir más formatos de una misma
obra? Si es así y si para ti crear un formato más requiere recursos y
tiempo adicionales, por desgracia, hay algo en tu metodología de trabajo
que provoque este problema. No se trata que una dificultad técnica, sino
que es un **conflicto metodológico**.

Aunque parece sencillo empezar a concebir a la edición como un método,
la reducción de estas tecnologías a su aspecto instrumental genera la
idea de que la solución ha de yacer en quienes la usan y la desarrollan.

Es decir, para la mayoría de los editores la solución queda afuera de su
esfera. Ahora quien programa es el que cuenta con la solución. Y esta
dependencia no es agradable porque por décadas el editor no había tenido
que recurrir a profesionales afuera de su campo para cumplir con sus
tareas.

### Subordinación exterior y ausencia de metodología

Eso no es lo peor, depender de profesionales externos a la edición
provoca:

1.  Una asistencia técnica a través de un pago de prestación de
    servicios que, por lo general, el editor no termina de comprender
    del todo, prestándose a diversos abusos.
2.  Un desinterés del agente externo al [cuidado
    editorial](https://marianaeguaras.com/el-cuidado-editorial-durante-la-produccion-de-un-libro/),
    ya que es común que trate las necesidades del editor como un
    problema solucionable a través de una vía técnica que, al menos para
    el caso de la lengua española, no cumple del todo con los estándares
    de calidad de su tradición.
3.  Una amargura por parte de quienes editan, porque se percatan de que
    su calidad editorial empieza a estar condicionada por el control
    técnico de una metodología, al menos en parte, distinta a los
    procesos tradicionales de publicación.

Es posible atajar estos problemas si se buscan mecanismos que expliciten
que **la edición es tradición, arte, profesión, pero también es
método**.

## Educación

La vía más evidente para comprender que la edición también es método es
la educación: que quienes editan empiecen a aprender que gran parte de
sus dificultades están relacionadas con:

-   las metodologías que utilizan en su trabajo, y
-   la ausencia de un panorama general sobre las posibilidades
    metodológicas para poder llevar a cabo su trabajo.

El sector editorial tiene un fuerte problema pedagógico. Por un lado, la
educación continua que se ofrece a estos profesionales implica altos
costos que no muchos pueden cubrir. ¿Cuántas veces hemos desistido de
acudir a capacitaciones, talleres, seminarios, coloquios, ferias o
estudios de grado porque no podemos pagarlo?

Sin embargo, el aspecto que, por el momento, quiero hacer hincapié es
cómo **la aproximación, por lo general, no es integral**.

¿Cuántas veces hemos asistido a eventos para aprender a utilizar *x* o
*y* programa, plataforma o lenguaje? ¿Cuántas veces hemos podido
incrustar esos conocimientos en un panorama más general?

¿Cuántas veces lo que se nos enseña es *software* propietario que
requiere de pago o suscripción? ¿Cuántas veces este *software* se nos
vende como la panacea a nuestros problemas?

**La formación que el editor necesita no es a base de *software***. Sí,
al final quien edita se las ve con el *software*, pero eso solo es el
corolario de un proceso pedagógico que requiere de fundamentos teóricos
y metodológicos.

### El aprendizaje tecnológico no se basa en dominar un *software*

Un grave error en nuestra educación es suponer que «aprender a usar la
computadora» es «aprender a usar *x* o *y* *software*». Comprobaremos
una y otra vez que lo aprendido es o será obsoleto ---hasta el punto de
una capacitación sin fin que, más que aclarar, nos confunde---. Pero,
con mayor lamento, nos daremos cuenta que al final carecemos de la
seguridad de que sabemos utilizar nuestra única herramienta posible de
trabajo.

Si estás cómodo con tu entorno de trabajo ---probablemente paquetería de
Microsoft o Adobe---, solo recuerda qué pasó cuando *la solución* y *el
método* editorial se reducían a programas como PageMaker o
QuarkXPress... ¿No fue grato tener que reaprender, cierto?

Una formación con un fuerte peso teórico y metodológico ayudaría a que
los editores, sin importar los programas o los formatos iniciales o
requeridos, puedan construir sus propias soluciones.

No se trata de aprender a usar *software* sino de tener un criterio
sobre lo que se necesita para llegar del punto A ---los archivos para la
edición--- al punto B ---los formatos finales de la obra---.

Al final, el gran prejuicio con el que se batalla es que los programas y
los formatos siempre son secundarios; que un formato no está asociado a
un programa determinado. Incluso más radical aún: todo formato final es
desechable.

Con una sólida preparación metodológica se rompe ese aire de complejidad
y ese gran distanciamiento entre los programas milagro y los formatos en
boga, y lo que el editor realmente necesita.

La educación que necesitamos no es a modo de brincos, de programa en
programa, para estar a la vanguardia. La educación que el editor
requiere es una paulatina profundización en su método, cuyo reflejo
técnico sea, precisamente, eso: una imagen, una muestra concreta de una
metodología que no está encadenada a programas o formatos.

### ¿Soluciones o creación de nuevas necesidades?

La confianza puede llegar a ser de tal grado que cada vez que se anuncia
una nueva plataforma o programa no es causa de desasosiego ni
desconcierto.

No es que la novedad sea despreciada, sino que se hace fácil distinguir
cuándo una herramienta en realidad viene a resolver una dificultad
metodológica y cuánto esta solo es
[*bluff*](https://en.wikipedia.org/wiki/Bluff_(poker)): un programa que
se declara como útil cuando no hace sino crear dependencia al editor sin
jamás resolver sus dificultades ---como el +++DRM+++---.

Por lo general, cuando un programa o plataforma nueva sale al mercado,
se promociona como *la* herramienta que soluciona *x* problema en el
ámbito de la edición. Sin embargo, muchas veces no resuelve gran cosa y,
en su lugar, crea una dependencia tecnológica del editor hacia ella.

Si esta situación se observara desde un panorama más integral podría
detectarse con más facilidad cuando estos programas o plataformas vienen
a solucionar algo o aparecen para crear una nueva necesidad.

## Laboratorios

El tiempo desperdiciado en saltar de programa en programa se convierte
en horas que ayudan a profundizar en los aspectos metodológicos. Este
escudriño puede evitar el mareo que de manera constante tiene el editor
frente a las nuevas tecnologías de la información. Conforme el editor se
empodere de su único medio de producción, se hace posible la
investigación y la experimentación.

¿Suena raro, no es así? ¿Cuándo habíamos escuchado sobre investigación o
experimentación editorial? La investigación relativa a la edición, por
lo general, tiene un fuerte énfasis histórico o bibliófilo. La
experimentación editorial, comúnmente, son ejercicios de prueba y error
sobre programas o formatos empleados en la edición.

La idea de investigación que aquí se plantea tiene un **fuerte énfasis
técnico y tecnológico.** Cuando quien edita empieza a tener un dominio
sobre su trabajo se hace posible ahondar en la reflexión sobre lo que
implica tener un único medio de producción.

### Cómo y con qué se están haciendo las cosas 

La investigación consistiría en pensar y repensar a cada instante cómo y
con qué se están haciendo las cosas y qué son esas cosas editoriales.

¿Por qué, como «gremio», usamos tales programas y no otros? ¿Por qué
este u otro formato es el más usado? ¿Cómo podemos automatizar las
cosas? ¿Cómo es posible encontrar lazos entre la edición estandarizada y
la que no lo es? En fin, ¿qué es esa cosa llamada *edición* cuando ha
desaparecido su correlato análogo ---el papel y la página--- del que
siempre dependió?

Estas solo son unas cuantas preguntas para un marco teórico que es mucho
más profundo que la constante discusión de los libros impresos contra
los digitales. La investigación no tiene su interés principal en
programas o formatos.

**La investigación es en torno a cómo pensar la edición** cuando todos
sus recursos han sido acaparados por el código y cómo ver al texto
cuando ya no solo es contenido, sino también estructura que no
necesariamente tiene que ser *estructura textual* (!).

La experimentación tiene una intencionalidad semejante. No consiste en
aplicar a prueba y error ciertas recetas. No es únicamente la
implementación de otros ecosistemas editoriales ajenos a los que usamos.

Esta consiste en **crear nuevos medios o herramientas para aumentar la
calidad editorial de nuestras publicaciones**. Pero no solo eso:
experimentar es partir del texto incluso para derivar en productos no
textuales (!).

Siendo editores nos cuesta mucho trabajo concebir la posibilidad de
productos editoriales no textuales a partir de nuestra principal base de
trabajo: el texto. Y seguirá sonando raro, porque el sector editorial
recién está viendo que casarse con el texto no implica casarse con las
publicaciones.

Como editor, ¿quieres publicar o quieres trabajar con el texto? Desde
que la edición se hizo código esto deriva en actividades muy distintas,
cuando durante siglos para la edición una era consecuencia de la otra.

### Laboratorio de investigación editorial

Cuando se cuenta con un espacio para la investigación y experimentación
lo que se tiene es un laboratorio; en este caso, un laboratorio
editorial.

El [Taller de Edición Digital](http://ted.perrotuerto.blog/) (+++TED+++) es
un ejemplo. Con un fuerte énfasis pedagógico, el +++TED+++ poco a poco
va mostrando su verdadera cara: no es un taller para aprender a editar,
sino un laboratorio editorial.

Pero, a pesar del optimismo, el +++TED+++ es frágil. Sin todas las
personas que organizan actividades paralelas al +++TED+++ este taller no
tendría vitalidad. ¿Es posible replicar espacios similares de
laboratorio editoriales?

Son las universidades, las instituciones gubernamentales y las
editoriales establecidas las que sin problemas podrían abrir
laboratorios en su estructura.

En la edición no es común hablar de esta clase de espacios. Sin embargo,
cuando quien edita es también el responsable de generar sus propias
metodologías o herramientas los laboratorios se fundan como una
infraestructura necesaria.

No podremos retomar las riendas de nuestra profesión hasta que no
concibamos la necesidad de estos espacios. **El empoderamiento
tecnológico y la independencia técnica del editor no basta con ser
individual**.

En colectivo es posible armar espacios de discusión que van más allá de
estrategias publicitarias o de añejos debates que solo exponen el grado
de desconocimiento hacia los métodos ya necesarios para publicar.

En laboratorios es posible establecer esos espacios que la edición
necesita. Ahí será posible que en sí misma satisfaga sus necesidades
presentes y futuras. Su quehacer es una actividad traducida en
publicaciones y texto, pero también en código y en elementos editoriales
aún desconocidos.

### ¿El texto como un aspecto definitorio de la edición?

Editores, aún no completamos el mapamundi de nuestro mundo: estamos aún
en una nueva etapa de descubrimientos. Aún nos encontramos en nuestro
siglo +++XVI+++, con nuevos mundos por descubrir. Tenemos un gran camino
por delante desde y más allá de lo que hemos conservado como tradición.

El sector y la tradición editoriales han cometido un error fundamental.
Ambos han supuesto que lo más importante o más común para la edición ya
ha sido descubierto y solo es preciso afinarlo o adaptarlo a nuevas
tecnologías y técnicas.

La realidad es que la edición ha perdido su carácter definitorio al no
estar ya anclada a los procesos análogos, el papel y la página que la
vieron nacer. Desde hace décadas lo que llamamos «editar» y «publicar»
requieren de una radical redefinición.

Esta será insuficiente si no se elimina de una vez por todas lo que se
considera el núcleo de la edición: el texto. La noción común del texto
como contenido, como un conjunto de líneas de texto, nunca ha sido, no
es ni podrá ser el aspecto definitorio de la edición. Desde siempre la
edición ha desbordado esta materialidad tan bruta y evidente, a la vez
que protocolaria.

## Apertura

Si la edición se hizo código, entonces también hay que *hackearla*. Bajo
este panorama quien edita ya no es un usuario o consumidor de
*software*. Es, asimismo, su arquitecto.

Para poder construir los espacios y las herramientas necesarias para
esta profesión se requiere acceso al código. Hay que recordar que una
fundamental diferencia entre las tecnologías análogas y las digitales es
que estas últimas precisan esconder su infraestructura para poder
funcionar.

Con diferentes grados de complejidad, la maquinaria análoga siempre
hacía posible observar su funcionamiento mientras se utilizaba. Sin
embargo, el *hardware* con verlo no nos da una idea del *software* que
está ejecutando. Pero tampoco el *software* permite observar su
constitución al utilizarlo.

El *software* se ejecuta y el desarrollador es el que decide qué
mostrarle al usuario a través de una interfaz. Si se pretende estudiar
su funcionamiento no solo se requiere hacer pautas entre la ejecución y
el análisis de las funciones, también es menester el acceso al código.

El sector editorial ha sido criticado constantemente en no ser
transparente en la difusión de sus conocimientos y técnicas. La
situación es más oscura si quien edita utiliza un *software* propietario
cuyo único acceso es el ejecutable; es decir, la limitación de solo
tener acceso al código máquina prácticamente inteligible para las
personas.

### Cambio de rumbo

La apertura se vuelve una necesidad para que el editor como programador
pueda ahondar con la investigación y la experimentación editoriales. No
es un capricho, como «gremio» no podremos mejorar si:

1.  Nos concebimos solo como usuarios y no como arquitectos de
    *software*.
2.  A nuestros pares los vemos como una competencia y a nuestros
    *software* y metodologías como la diferencia que nos da mayor
    competitividad.
3.  Nuestra labor de investigación y experimentación permanece cerrada.

Esto provoca que nos sentemos a pensar **cómo es posible una educación
integral y la gestación de laboratorios** donde su financiamiento no
depende directamente de la capacidad adquisitiva de sus asistentes o
integrantes. La tarea no es fácil, pero el giro digital en la edición no
es imposible: ya estamos en eso.

<aside class="espacio-arriba2">

###### Sobre esta entrada

* Título original: «Un giro digital en la edición».
* Título en el _blog_ de Mariana: «Un giro digital en la edición y una propuesta sobre metodología editorial».
* Fecha de publicación: 2 de octubre del 2018.
* [Redacción original](https://gitlab.com/NikaZhenya/entradas-eguaras/blob/master/23%20-%20Un%20giro%20digital%20en%20la%20edici%C3%B3n/entrada.md).
* [Revisión de Mariana](https://marianaeguaras.com/un-giro-digital-en-la-edicion-y-una-propuesta-sobre-metodologia-editorial/).

</aside>
